/*
 * RRTS_Planner.cpp
 *
 *  Created on: 10 Aug, 2014
 *      Author: liuwlz
 */




#include <OMPLRRTS/RRTS_Planner.hpp>

namespace MotionPlan {

	RRTSPlanner::
	RRTSPlanner(int spaceType):
	plan_status_(Planning),
	pri_nh_("~"),
	space_type_(spaceType),
	plan_space_init_(false),
	goal_init_(false),
	metric_map_init_(false),
	initial_unsafe_init_(false),
	initial_plan_init_(false),
	optimal_metric_(DBL_MAX){

		pri_nh_.param("global_frame",global_frame_,string("map"));
		pri_nh_.param("base_frame",base_frame_,string("base_link"));
		pri_nh_.param("turning_radius",turning_radius_,3.0);
		pri_nh_.param("path_unsafe_tolerance", path_unsafe_tolerance_, 1.0);
		pri_nh_.param("plan_duration_tolerance",plan_duration_tolerance_,1.5);
		pri_nh_.param("timer_duration",timer_duration_,0.05);

		metric_map_ = boost::shared_ptr<Metric_Map::MetricMapMsg>(new Metric_Map::MetricMapMsg());
		bounds_ = boost::shared_ptr<ob::RealVectorBounds>(new ob::RealVectorBounds(2));

		plan_timer_ = nh_.createTimer(ros::Duration(timer_duration_),&RRTSPlanner::planTimer,this);
		metric_map_sub_ = nh_.subscribe("metric_map",1,&RRTSPlanner::metricMapCallBack,this);
		rviz_goal_sub_ = nh_.subscribe("rrts_goal",1,&RRTSPlanner::rvizGoalCallBack, this);
	}

	RRTSPlanner::
	~RRTSPlanner(){

	}

	void
	RRTSPlanner::
	initPlanSpace(){
		switch (space_type_){
		case 1: /*Dubins Symmetric: Forward and Backward */
			ROS_INFO("Dubins Forward and Backward");
			space_ = ob::StateSpacePtr(new ob::DubinsStateSpace(turning_radius_, true));
			space_->setName(string("DubinsSym"));
			break;
		case 2: /*ReedShepp: Forward and Backward*/
			ROS_INFO("ReedShepp");
			space_ = ob::StateSpacePtr(new ob::ReedsSheppStateSpace(turning_radius_));
			space_->setName(string("ReedShepp"));
			break;
		case 3: /*Dubins: Forward only*/
			ROS_INFO("Dubins Forward");
			space_ = ob::StateSpacePtr(new ob::DubinsStateSpace(turning_radius_, false));
			space_->setName(string("Dubins"));
			break;
		default:
			break;
		}
		updatePlanBound();
		rrts_planner_ = boost::shared_ptr<OMPLRRTS>(new OMPLRRTS(space_));
		plan_space_init_ = true;
	}

	void
	RRTSPlanner::
	updatePlanBound(){
		vector<geometry_msgs::Pose> base_poses(4);
		double flag = 1.0;
		for (vector<geometry_msgs::Pose>::iterator iter = base_poses.begin(); iter != base_poses.end(); iter ++){
			if (iter - base_poses.begin()<2){
				iter->position.x = flag * metric_map_->info.width * metric_map_->info.resolution/2.0;
				iter->position.y  =flag * metric_map_->info.height * metric_map_->info.resolution/2.0;
			}
			else{
				iter->position.x = flag * metric_map_->info.width * metric_map_->info.resolution/2.0;
				iter->position.y = -flag * metric_map_->info.height * metric_map_->info.resolution/2.0;
			}
			iter->orientation.w = 1.0;
			flag = -flag;
		}
		bounds_->setHigh(0.0);
		bounds_->setLow(DBL_MAX);
		for (vector<geometry_msgs::Pose>::iterator iter = base_poses.begin(); iter != base_poses.end(); iter ++){
			geometry_msgs::Pose global_pose;
			transformToGlobalFrame(*iter, global_pose);
			bounds_->low[0] = global_pose.position.x < bounds_->low[0] ? global_pose.position.x : bounds_->low[0];
			bounds_->low[1] = global_pose.position.y < bounds_->low[1] ? global_pose.position.y : bounds_->low[1];
			bounds_->high[0] = global_pose.position.x > bounds_->high[0] ? global_pose.position.x : bounds_->high[0];
			bounds_->high[1] = global_pose.position.y > bounds_->high[1] ? global_pose.position.y : bounds_->high[1];
		}
		if (RRTSDebug){
			ROS_INFO("Bound Low[0]: %f, Low[1]: %f, High[0]: %f, High[1]: %f", bounds_->low[0],bounds_->low[1],bounds_->high[0],bounds_->high[1] );
		}
		if (!plan_space_init_){
			space_->as<ob::SE2StateSpace>()->setBounds(*bounds_);
		}
		else{
			rrts_planner_->resetBounds(*bounds_);
		}
	}

	void
	RRTSPlanner::
	rvizGoalCallBack(const geometry_msgs::PoseStamped& goalIn){
		updateGoal(goalIn);
	}

	void
	RRTSPlanner::
	updateGoal(const geometry_msgs::PoseStamped& goalIn){
		goal_init_ = true;
		goal_ = goalIn;
	}

	void
	RRTSPlanner::
	updateRoot(){
		geometry_msgs::Pose base_pose_tmp, global_pose_tmp;
		base_pose_tmp.position.x = 0.0;
		base_pose_tmp.position.y = 0.0;
		base_pose_tmp.orientation.w = 1.0;

		if (!transformToGlobalFrame(base_pose_tmp,vehicle_pose_.pose))
			ROS_ERROR("Faild to get vehicle pose");
		vehicle_pose_.header.stamp = ros::Time::now();
		vehicle_pose_.header.frame_id = global_frame_;
	}

	void
	RRTSPlanner::
	updateTask(){
		if (true)
			ROS_INFO("Update Planning Task");
		updateRoot();
		vector<double>root;
		root.push_back(vehicle_pose_.pose.position.x);
		root.push_back(vehicle_pose_.pose.position.y);
		root.push_back(getYawofPose(vehicle_pose_.pose));
		rrts_planner_->resetRoot(root);

		vector<double> goal;
		goal.push_back(goal_.pose.position.x);
		goal.push_back(goal_.pose.position.y);
		goal.push_back(getYawofPose(goal_.pose));
		rrts_planner_->resetGoal(goal);

		updatePlanBound();
		updatePlannerMetricMap(true);
	}

	void
	RRTSPlanner::
	planTimer(const ros::TimerEvent& e){
		ROS_INFO("Iterate");
		if (!plan_space_init_ || !metric_map_init_)
			return;
		if (goal_init_){
			try{
			    updateTask();
			}
			catch(std::exception& e){
			    ROS_ERROR("Update Task Error: RRTS_Planner.cpp line 167");
			    return;
			}
			goal_init_ = false;
		}
		double plan_duration=0.0, unsafe_duration = 0.0;
		plan_status_ = Planning;
		updatePlannerMetricMap(false);
		optimal_metric_ = rrts_planner_->getLowerBoundMetric();
		if (optimal_metric_ > DBL_MAX/2){
			ROS_INFO("Planning in Progress");
			rrts_planner_->iteratePlan(timer_duration_*0.9);
			if (!initial_plan_init_){
				initial_plan_time_ = ros::Time::now();
				initial_plan_init_ = true;
			}
			else{
				plan_duration = (ros::Time::now()-initial_plan_time_).toSec();
			}
			if (plan_duration > plan_duration_tolerance_){
				ROS_WARN("Timeout over plan tolerance");
				updateTask();
				initial_plan_init_ = false;
			}
		}
		else if (optimal_metric_ > 0 && optimal_metric_ <= DBL_MAX/2){
			initial_plan_init_ = false;
			if (space_type_ == 3){
				if (rrts_planner_->getViewer()->checkDubinsSolutionPath() == 0){
					ROS_WARN("Reverse Path bug from OMPL, Replan");
					plan_status_ = Planning;
					updateTask();
					return;
				}
			}
			rrts_planner_->getViewer()->publishSolutionPath();
			if (rrts_planner_->lazyCheckTree()){
				plan_status_ = PathGot;
				initial_unsafe_init_ = false;
			}
			else{
				if (!initial_unsafe_init_){
					initial_unsafe_time_ = ros::Time::now();
					initial_unsafe_init_ = true;
					ROS_INFO("Start unsafe Timer: Current Time : %f", initial_unsafe_time_.toSec());
				}
				else{
					unsafe_duration = (ros::Time::now()-initial_unsafe_time_).toSec();
				}
				if (unsafe_duration > path_unsafe_tolerance_){
					ROS_INFO("Start unsafe Trigger: Current Time : %f", ros::Time::now().toSec());
					plan_status_ = PathUnsafe;
					updateTask();
					initial_unsafe_init_ = false;
				}
				else{
					plan_status_ = PathGot;
				}
			}
		}
		else{
			ROS_WARN("Unknown Plan status");
		}
	}

	void
	RRTSPlanner::
	metricMapCallBack(const Metric_Map::MetricMapMsgPtr metricMapIn){
		if (!metric_map_init_)
			cout <<"RRT* Metric Map Got"<<endl;
		metric_map_ = metricMapIn;
		transformToGlobalFrame(metricMapIn->info.origin ,metric_map_->info.origin);
		metric_map_->info.origin.position.z = getYawofPose(metric_map_->info.origin);
		if(!plan_space_init_)
			initPlanSpace();
		metric_map_init_ = true;
	}

	void
	RRTSPlanner::
	updatePlannerMetricMap(bool resetTask){
		rrts_planner_->resetMetricMap(metric_map_,resetTask);
	}

	double
	RRTSPlanner::
	getYawofPose(const geometry_msgs::Pose& poseIn){
		double roll, pitch, yaw;
		tf::Quaternion quaternion;
		tf::quaternionMsgToTF(poseIn.orientation, quaternion);
		tf::Matrix3x3 rotation(quaternion);
		rotation.getRPY(roll, pitch, yaw);
		return yaw;
	}

	bool
	RRTSPlanner::
	transformToGlobalFrame(const geometry_msgs::Pose& poseIn, geometry_msgs::Pose& poseOut){
		geometry_msgs::PoseStamped poseStampIn, poseStampOut;
		poseStampIn.header.stamp = ros::Time();
		poseStampIn.header.frame_id = base_frame_;
		poseStampIn.pose = poseIn;
		poseStampOut.header.stamp = ros::Time();
		poseStampOut.header.frame_id = global_frame_;

		try {
			tf_.waitForTransform(global_frame_,base_frame_,ros::Time(0),ros::Duration(0.01));
			tf_.transformPose(global_frame_, poseStampIn, poseStampOut);
		}
		catch(tf::LookupException& ex) {
			ROS_ERROR("No Transform available Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ConnectivityException& ex) {
			ROS_ERROR("Connectivity Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ExtrapolationException& ex) {
			ROS_ERROR("Extrapolation Error: %s\n", ex.what());
			return false;
		}
		poseOut = poseStampOut.pose;

		return true;
	}

}  // namespace MotionPlan
