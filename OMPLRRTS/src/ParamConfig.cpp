/*
 * ParamConfig.cpp
 *
 *  Created on: 1 Apr, 2014
 *      Author: liuwlz
 */

#include <OMPLRRTS/ParamConfig.h>

namespace MotionPlan{
	ParamConfig::ParamConfig(){
	  
#if 1
		//For golfcart
		robotParam[robotHeight] = 2.28;
		robotParam[robotWidth] = 1.2;
		robotParam[wheelDist] = 0.45;
		robotParam[safeMargin] = 0.2;
#else
		//For iMiev
		robotParam[robotHeight] = 2.55;
		robotParam[robotWidth] = 1.5;
		robotParam[wheelDist] = 0.5;
		robotParam[safeMargin] = 0.3;
		
#endif
	}

	ParamConfig::~ParamConfig(){

	}
}
