/*
 * RvizGoal.h
 *
 *  Created on: 3 Apr, 2014
 *      Author: liuwlz
 */

#ifndef RVIZGOAL_H_
#define RVIZGOAL_H_

#include <iostream>
#include <ctime>

#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Path.h>
#include <tf/tf.h>

namespace MotionPlan{
	class RvizGoal{
		ros::NodeHandle nh_;
		ros::Subscriber rviz_goal_sub_;
		ros::Publisher rviz_goal_pub_;
	public:
		RvizGoal();
		virtual ~RvizGoal();
		void rvizGoalCallBack(const geometry_msgs::PoseStamped::ConstPtr& goalIn);
	};
}

#endif /* RVIZGOAL_H_ */
