/*
 * RRTS_Dubins.hpp
 *
 *  Created on: 16 Apr, 2015
 *      Author: liuwlz
 */

#ifndef INCLUDE_OMPLRRTS_RRTS_DUBINS_HPP_
#define INCLUDE_OMPLRRTS_RRTS_DUBINS_HPP_

#include <OMPLRRTS/RRTS_Planner.hpp>

namespace MotionPlan {

	class RRTSDubins:public RRTSPlanner {
	public:
		RRTSDubins():RRTSPlanner(3){

		}

		virtual ~RRTSDubins(){

		}

		string getPlannerName(){return string("RRTS_Dubins");}

	private:

	};


}  // namespace MotionPlan



#endif /* INCLUDE_OMPLRRTS_RRTS_DUBINS_HPP_ */
