/*
 * CollisionChecker.h
 *
 *  Created on: 31 Mar, 2014
 *      Author: liuwlz
 *
 *  Implementation of collision checker: Inherited from OMPL StateValidityChecker and using MetricMap info to indicate occupancy
 */

#ifndef COLLISIONCHECKER_H_
#define COLLISIONCHECKER_H_

#include <float.h>
#include <math.h>
#include <stdlib.h>

#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/SpaceInformation.h>

#include <OMPLRRTS/ParamConfig.h>

#include <Metric_Map/MetricMapMsg.h>

#define NAIVECHECKING false

namespace ob = ompl::base;

using namespace std;
using namespace boost;

namespace MotionPlan {

	/**\brief CollisionChecker class inherited from StateValidityChecker for state collision checking*/
	class CollisionChecker:public ob::StateValidityChecker {

		typedef ob::State state_t;
		typedef ob::RealVectorStateSpace::StateType realVectorState_t;
		typedef ob::SE2StateSpace::StateType SE2State_t;
		typedef boost::shared_ptr<Metric_Map::MetricMapMsg> metric_map_ptr;

		metric_map_ptr metric_map_; /**< Metric map pointer*/
		boost::shared_ptr<ParamConfig> config_; /**< Parameters object*/

		double width, height, dist_rear, safety_gap; /**< Vehicle's geometrical model parameters*/

	public:
		/**
		 * Constructor
		 * @param si: Spaceinformation pointer
		 * @param distMap: Clearance map pointer input
		 */
		CollisionChecker(const ob::SpaceInformationPtr& si, const metric_map_ptr& metricMap);

		/**
		 * Destructor
		 */
		virtual ~CollisionChecker();

		/**
		 * Checking the validity of input state stateIn
		 * @param stateIn: Input state
		 * @return True if the stateIn is collision free, else False
		 */
		virtual bool isValid(const state_t* stateIn) const;

		/**
		 * Calculate the clearance of stateIn
		 * @param stateIn: Input state
		 * @return The state clearance
		 */
		double clearance(const state_t* stateIn) const;

		/**
		 * Called by function clearance() to compute the clearance of stateIn
		 * @param stateIn: Input state
		 * @return The state clearance
		 */
		double getClearance(const SE2State_t* stateIn)const;

		/**
		 * Get the index of (x,y) that resides in metric_map_
		 * @param x, y: Input position
		 * @param indexOut: Output index
		 * @return: 1
		 */
		int getStateIndex(const double& x, const double& y, int& indexOut)const;


		/**
		 * Transform the input state to MetricMap frame for clearance checking
		 * @param stateIn: Input state
		 * @param poseOut: Output pose
		 */
		void transformToMetricMapFrame(const SE2State_t* stateIn, vector<double>& poseOut) const;
	};
}  // namespace MPAV

#endif /* COLLISIONCHECKER_H_ */
