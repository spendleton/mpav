/*
 * RRTS_ReedShepp.hpp
 *
 *  Created on: 16 Apr, 2015
 *      Author: liuwlz
 */

#ifndef INCLUDE_OMPLRRTS_RRTS_REEDSHEPP_HPP_
#define INCLUDE_OMPLRRTS_RRTS_REEDSHEPP_HPP_

#include <OMPLRRTS/RRTS_Planner.hpp>

namespace MotionPlan {

	class RRTSReedShepp:public RRTSPlanner {
	public:
		RRTSReedShepp():RRTSPlanner(2){

		}

		~RRTSReedShepp(){

		}

		string getPlannerName(){return string("RRTS_ReedShepp");};

	private:
	};


}  // namespace MotionPlan




#endif /* INCLUDE_OMPLRRTS_RRTS_REEDSHEPP_HPP_ */
