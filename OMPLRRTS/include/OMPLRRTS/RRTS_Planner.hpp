/*
 * RRTS_Planner.hpp
 *
 *  Created on: 10 Aug, 2014
 *      Author: liuwlz
 */

#ifndef RRTS_PLANNER_HPP_
#define RRTS_PLANNER_HPP_

#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/PoseStamped.h>

#include <tf/tf.h>
#include <tf/transform_listener.h>

#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/ReedsSheppStateSpace.h>
#include <ompl/base/spaces/DubinsStateSpace.h>
#include <ompl/base/spaces/SE2StateSpace.h>

#include <Metric_Map/MetricMapMsg.h>

#include <OMPLRRTS/OMPLRRTS.h>

#define RRTSDebug false

namespace MotionPlan {

	class RRTSPlanner {

	public:
		RRTSPlanner(int spaceType);
		virtual ~RRTSPlanner();

		ros::Timer plan_timer_;

		typedef enum{Planning,PathGot,PathUnsafe} PlanStatusType;
		PlanStatusType plan_status_;

		void updateGoal(const geometry_msgs::PoseStamped& goalIn);
		nav_msgs::Path getSolutionPath()const {return rrts_planner_->getSolutionPath();};
		void resetPlannerStatus(){optimal_metric_ = DBL_MAX; plan_status_ = Planning; initial_plan_init_ = false;};

	private:
		ros::NodeHandle nh_, pri_nh_;
		ros::Subscriber metric_map_sub_, rviz_goal_sub_;;

		tf::TransformListener tf_;

		geometry_msgs::PoseStamped goal_, vehicle_pose_;
		Metric_Map::MetricMapMsgPtr metric_map_;

		boost::shared_ptr<OMPLRRTS> rrts_planner_;
		ob::StateSpacePtr space_;
		boost::shared_ptr<ob::RealVectorBounds> bounds_;

		int space_type_;

		bool plan_space_init_, goal_init_, metric_map_init_;
		bool initial_unsafe_init_, initial_plan_init_;
		ros::Time initial_unsafe_time_, initial_plan_time_;

		string base_frame_, global_frame_;
		double turning_radius_;
		double optimal_metric_;
		double path_unsafe_tolerance_, plan_duration_tolerance_;
		double timer_duration_;

		void initPlanSpace();
		void updatePlanBound();
		void updateRoot();
		void updateTask();

		void updatePlannerMetricMap(bool resetTask);
		void planTimer(const ros::TimerEvent& e);
		void metricMapCallBack(const Metric_Map::MetricMapMsgPtr metricMapIn);
		void rvizGoalCallBack(const geometry_msgs::PoseStamped& goalIn);

		double getYawofPose(const geometry_msgs::Pose& poseIn);
		bool transformToGlobalFrame(const geometry_msgs::Pose& poseIn, geometry_msgs::Pose& poseOut);
	};

}  // namespace MotionPlan

#endif /* RRTS_PLANNER_HPP_ */
