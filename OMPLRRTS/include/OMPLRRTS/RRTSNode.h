/*
 * RRTS_Node.h
 *
 *  Created on: 2 Apr, 2014
 *      Author: liuwlz
 */

#ifndef RRTS_NODE_H_
#define RRTS_NODE_H_

#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/OccupancyGrid.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/PoseStamped.h>

#include <tf/tf.h>
#include <tf/transform_listener.h>

#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/ReedsSheppStateSpace.h>
#include <ompl/base/spaces/DubinsStateSpace.h>
#include <ompl/base/spaces/SE2StateSpace.h>

#include <OMPLRRTS/OMPLRRTS.h>
#include <Metric_Map/MetricMapMsg.h>

using namespace std;
using namespace boost;

namespace MotionPlan{

	typedef geometry_msgs::PoseStamped pose_t;
	typedef geometry_msgs::PoseStampedPtr pose_ptr;
	typedef geometry_msgs::Point32 point_t;
	typedef geometry_msgs::Point32Ptr point_ptr;
	typedef sensor_msgs::PointCloudPtr point_clound_ptr;
	typedef shared_ptr<Metric_Map::MetricMapMsg> metric_map_ptr;
	typedef OMPLRRTS rrts_t;
	typedef shared_ptr<rrts_t> rrts_ptr;

	/**\brief Interface class for RRTS*/
	class RRTSNode{

		ros::NodeHandle nh_, private_nh_;

		ros::Publisher plan_map_pub_;

		ros::Subscriber goal_sub_, dist_map_sub_, dist_map_pts_sub_;
		ros::Timer plan_timer_, plan_frame_tf_planner_;

		tf::StampedTransform tf_transform_;
		tf::TransformListener tf_listener_;
		tf::TransformBroadcaster tf_broadcaster_;

		pose_ptr global_goal_;
		point_clound_ptr dist_map_pts;
		metric_map_ptr metric_map_;
		rrts_ptr planner_;

		int space_type_;
		double best_metric;
		bool is_planner_space_set_, is_goal_set_, is_plan_frame_set_, is_path_committed_;
		string global_frame_, base_frame_, plan_frame_, local_frame_;

	public:
		RRTSNode(int spaceType);
		virtual ~RRTSNode();

		int initPlanSpace(const metric_map_ptr& mapIn);

		int updateRoot();
		int updateGoal(const pose_ptr& goalIn);
		int updatePlannerDistMap(bool resetTask);
		int updateTask(const point_ptr& rootIn, const point_ptr& goalIn);

		void planTimer(const ros::TimerEvent & e);
		void goalCallBack(const pose_ptr& goalIn);
		void distMapCallBack(const dist_map_ptr& distMapIn);
		void distMapPointsCallBack(const point_clound_ptr& distMapPtsIn);
		void planFrameTFTimer(const ros::TimerEvent &e);

		int setStaticPlanFram();
		int getVehiclePose(point_t& rootOut);
		int transformToLocalFrame(const pose_ptr& poseIn, point_ptr& pointOut);

		int publishPlanMap(const nav_msgs::OccupancyGrid& mapIn);

	};
}

#endif /* RRTS_NODE_H_ */
