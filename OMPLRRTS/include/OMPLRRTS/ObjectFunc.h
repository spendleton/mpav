/*
 * ObjectFunc.h
 *
 *  Created on: 31 Mar, 2014
 *      Author: liuwlz
 */

#ifndef OBJECTFUNC_H_
#define OBJECTFUNC_H_

#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/base/spaces/ReedsSheppStateSpace.h>

#include <ompl/base/objectives/StateCostIntegralObjective.h>

namespace ob = ompl::base;

using namespace std;

namespace MotionPlan {

	typedef ob::ReedsSheppStateSpace::ReedsSheppPath RS_path_t;

	/**\brief Object Function, inherited from class StateCostIntegralObjective*/
	class ObjectFunc:public ob::StateCostIntegralObjective{

		ob::SpaceInformationPtr si_; /**<Spaceinformation Pointer*/
		double bwd_penalty_ratio_; /**<Backward driving penalty ratio*/

	public:

		/**
		 * Constructor
		 * @param si: spaceinfomation_pointer
		 * @param checkMotion: Flag to indicate whether check the state cost along the entire edge
		 * @param bwd_penalty_ratio: Backward driving penalty gain relative to forward driving
		 */
		ObjectFunc(const ob::SpaceInformationPtr& si, bool checkMotion, double bwd_penalty_ratio);

		/**
		 * Destructor
		 */
		virtual ~ObjectFunc();

		/**
		 * Get the state cost associated with stateIn based on the clearance map
		 * @param stateIn: Input state to be evaluated
		 * @return Cost with type ompl::base::Cost
		 */
		ob::Cost stateCost(const ob::State* stateIn) const;

		/**
		 * Get the edge cost associated with edge start from s1 to s2
		 * @param s1: Initial state of the edge
		 * @param s2: End state of the edge
		 * @return Cost with type ompl::base::Cost
		 */
		ob::Cost motionCost(const ob::State* s1, const ob::State* s2) const;

		/**
		 * Interpolate the state stateOut which locates at t*pathIn.length().
		 * @param stateFrom: Initial state of pathIn
		 * @param pathIn: ReedShepp path
		 * @param t: Segment index that within [0,1]
		 * @param stateOut: Output state which stores the pose and the control flag
		 * @return: 1
		 */
		int interpolateRSPath(const ob::State* stateFrom, const RS_path_t& pathIn, double t, ob::State& stateOut) const;
	};
}  // namespace MPAV


#endif /* OBJECTFUNC_H_ */
