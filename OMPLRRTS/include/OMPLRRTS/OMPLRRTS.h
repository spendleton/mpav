/*
 * OMPLRRTS.h
 *
 *  Created on: 28 Mar, 2014
 *      Author: liuwlz
 */

#ifndef OMPLRRTS_H_
#define OMPLRRTS_H_

#include <ompl/base/spaces/ReedsSheppStateSpace.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/DubinsStateSpace.h>

#include <ompl/base/SpaceInformation.h>
#include <ompl/base/ScopedState.h>
#include <ompl/base/PlannerStatus.h>

#include <ompl/base/samplers/MaximizeClearanceValidStateSampler.h>
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>
#include <ompl/base/PlannerData.h>

#include <ompl/datastructures/NearestNeighborsSqrtApprox.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/planners/rrt/RRT.h>

#include <OMPLRRTS/CollisionChecker.h>
#include <OMPLRRTS/ObjectFunc.h>
#include <OMPLRRTS/Visualizer.h>

#include <Metric_Map/MetricMapMsg.h>

#include <fstream>
#include <float.h>

namespace ob = ompl::base;
namespace og = ompl::geometric;

using namespace std;
using namespace boost;

namespace MotionPlan{

	typedef ob::StateSpacePtr space_t;
	typedef ob::StateSpacePtr space_ptr;
	typedef ob::SpaceInformation spaceinfo_t;
	typedef ob::SpaceInformationPtr spaceinfo_ptr;
	typedef ob::ProblemDefinition problemDef_t;
	typedef ob::ProblemDefinitionPtr problemDef_ptr;
	typedef ob::ScopedState<> state_t;
	typedef ob::ScopedStatePtr state_ptr;
	typedef ob::OptimizationObjective object_t;
	typedef ob::OptimizationObjectivePtr object_ptr;
	typedef ob::PathPtr path_ptr;
	typedef ob::PlannerPtr planner_ptr;
	typedef ob::PlannerData plandata_t;
	typedef ob::PlannerDataPtr plandata_ptr;

	typedef boost::shared_ptr<Metric_Map::MetricMapMsg> metric_map_ptr;

	class OMPLRRTS{

		space_ptr space_;
		spaceinfo_ptr spaceinfo_;
		problemDef_ptr pdef_;
		state_ptr root_;
		state_ptr goal_;
		planner_ptr planner_;
		plandata_ptr plan_data_;
		metric_map_ptr metric_map_;
		boost::shared_ptr<Visualizer> viewer_;

		int planner_states_num_, vertex_bound_num_;
		double lower_bound_metric_;
		double cost_prefer_ratio_, bwd_penalty_ratio_;
		bool planner_reset_;

		object_ptr getBalancedObjective();
		ob::PlannerStatus plan_status_;

	public:

		OMPLRRTS(space_ptr space);
		virtual ~OMPLRRTS();

		int iteratePlan(double duration);
		void setupPlanner();
		void resetPlanner();
		bool lazyCheckTree();

		void resetMetricMap(const metric_map_ptr& metricMapIn, bool resetPlannerFlag);
		void resetRoot(const vector<double>& rootIn);
		void resetGoal(const vector<double>& goalIn);
		void resetBounds(const ob::RealVectorBounds& boundsIn);

		void setPlannerParams(double goal_bias, double steer_dist);
		double getLowerBoundMetric()const{return lower_bound_metric_;};
		int getPlannerStatesNum()const{return planner_states_num_;};
		nav_msgs::Path getSolutionPath()const;
		boost::shared_ptr<Visualizer> getViewer()const{ return viewer_;};
	};
}

#endif /* OMPLRRTS_H_ */
