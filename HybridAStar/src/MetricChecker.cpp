/*
 * MetricChecker.cpp
 *
 *  Created on: 30 Mar, 2015
 *      Author: liuwlz
 */

#include <HybridAStar/MetricChecker.hpp>


namespace MotionPlan{

	MetricChecker::
	MetricChecker():
	metric_initialzied_(false){
		veh_model_.resize(4);
		veh_model_[Height] = 2.28; veh_model_[Width] = 1.2;
		veh_model_[DistRear] = 0.45; veh_model_[SafetyGap] = 0.2;

		metric_map_ = shared_ptr<Metric_Map::MetricMapMsg>(new Metric_Map::MetricMapMsg());
	}

	MetricChecker::
	~MetricChecker(){

	}

	double
	MetricChecker::
	getMetric(const vector<double>& stateIn) const{
		assert(metric_initialzied_);
		double dist_metric = getClearance(stateIn);
		return dist_metric == 0 ? DBL_MAX : 1.0/dist_metric;
	}

	double
	MetricChecker::
	getClearance(const vector<double>& stateIn)const{
		vector<double> map_pose;
		this->transformToMetricMapFrame(stateIn,map_pose);
		double xc = map_pose[0], yc = map_pose[1], yaw = map_pose[2];
#ifdef LAZYCHECK
		int index = -1;
		if(getStateIndex(xc, yc, index) == 1){
			return metric_map_->metric[index];
		}
		else
			return 0.0;

#else
		double cos_yaw = cos(yaw), sin_yaw = sin(yaw);
		double min_clearance = DBL_MAX;

		double cxmax =  veh_model_[Height] - veh_model_[DistRear] + veh_model_[SafetyGap] + 0.001;
		double cymax = veh_model_[Width]/2.0 + veh_model_[SafetyGap] + 0.001;
		double cy = -veh_model_[Width]/2.0 - veh_model_[SafetyGap];

		//Check each pose within the vehicle geometric footprint.
		while(cy < cymax){
			double cx = -veh_model_[DistRear] - veh_model_[SafetyGap];
		    while(cx < cxmax){
		    	double x = xc + cx*cos_yaw - cy*sin_yaw;
		    	double y = yc + cx*sin_yaw + cy*cos_yaw;
		    	int index = -1;
		    	if(getStateIndex(x, y, index) == 1){
		    		double temp_clearance = metric_map_->metric[index];
		    		if (temp_clearance == 0.0)
		    			return 0.0;
		    		min_clearance = temp_clearance < min_clearance ? temp_clearance : min_clearance;
		    	}
		    	else{
		    		return 0.0;
		    	}
		    	cx = cx + metric_map_->info.resolution;
		    }
		    cy = cy + metric_map_->info.resolution;
		}
		return min_clearance;
#endif
	}

	int
	MetricChecker::
	getStateIndex(const double& x, const double& y, int& indexOut)const{
		int index_x = (int)round(x/metric_map_->info.resolution);
		int index_y = (int)round(y/metric_map_->info.resolution);

		if( (index_x < 0) || (index_x >= (int)metric_map_->info.width))
			return 0;
		if ( (index_y < 0) || (index_y >= (int)metric_map_->info.height))
			return 0;
		indexOut = index_y*metric_map_->info.width + index_x;
		return 1;
	}

	void
	MetricChecker::
	transformToMetricMapFrame(const vector<double>& stateIn, vector<double>& poseOut) const{
		poseOut.resize(3);
		double cos_map_yaw = cos(metric_map_->info.origin.position.z);
		double sin_map_yaw = sin(metric_map_->info.origin.position.z);

		poseOut[0] = (stateIn[0] - metric_map_->info.origin.position.x)*cos_map_yaw + (stateIn[1] - metric_map_->info.origin.position.y)*sin_map_yaw;
		poseOut[1] = -(stateIn[0] - metric_map_->info.origin.position.x)*sin_map_yaw + (stateIn[1]  - metric_map_->info.origin.position.y)*cos_map_yaw;
		poseOut[2] = stateIn[2] - metric_map_->info.origin.position.z;
	}


}  // namespace MotionPlan
