/*
 * HybridAstar.cpp
 *
 *  Created on: 24 Mar, 2015
 *      Author: liuwlz
 */

#include <HybridAStar/HybridAstar.hpp>


namespace MotionPlan {

	HybridAStar::
	HybridAStar():
	steering_limit_(45*M_PI/180),
	steering_resolution_(5*M_PI/180),
	steering_cost_(0.0),
	step_length_(0.5),
	discretize_ratio_(0.1),
	step_cost_forward_(1.0),
	step_cost_backward_(1.0),
	cost_discount_(0.99){
		metric_checker_ = shared_ptr<MetricChecker>(new MetricChecker());
		setupPlanner();
	}

	HybridAStar::
	~HybridAStar(){

	}

	void
	HybridAStar::
	setupPlanner(){
		ROS_INFO("Setup Hybrid A* Planner");
		//Initialize the steering primitives
        steerings_.push_back(0.0);
        for(double steering = steering_resolution_; steering <= steering_limit_; steering += steering_resolution_) {
            steerings_.push_back(-steering);
            steerings_.push_back(steering);
        }
        steps_.push_back(step_length_);
        steps_.push_back(-step_length_);
	}

    void
	HybridAStar::
	resetGoal(const State& goalIn) {
        this->goal_ = goalIn;
    }

    void
	HybridAStar::
	resetStart(const State& startIn) {
        this->start_ = startIn;
        this->start_[2] = boundAngle(this->start_[2] );
    }

    bool
    HybridAStar::
	searchPath(double durationIn, vector<State>& solutionPathOut, vector<int>& shiftOut) {
		ROS_INFO("Start to calculate path from [%f, %f, %f] to [%f, %f, %f]", start_[0], start_[1],start_[2], goal_[0], goal_[1],goal_[2]);
		double start_time = ros::Time::now().toSec();
		segments.clear();
        set<DiscreteState> closed_set;
        set<DiscreteState> open_set;
        priority_queue<QPair, vector<QPair>, greater<QPair> > priority_queue;

        PathSegment seg_init = {start_, 0, heuristic(start_), 0, -1, 1.0, 0};
		segments.push_back(seg_init);
        //visited.insert(discretize(start_));
        priority_queue.push(QPair(seg_init.cost+seg_init.heuristic, seg_init.index));
        open_set.insert(discretize(seg_init.state));

        bool found_solution = false;
        PathSegment goal_seg;

        while(!priority_queue.empty()) {
        	QPair queue_pair = priority_queue.top();
        	priority_queue.pop();
            PathSegment path_seg_tmp = segments[queue_pair.second];
            open_set.erase(discretize(path_seg_tmp.state));
            if(distToGoal(path_seg_tmp.state) < GOAL_TOLERANCE){
            	//cout << "state angle: "<<path_seg_tmp.state[2]<<" goal angle: "<<goal_[2]<< endl;
            	if (angleDist(path_seg_tmp.state[2], goal_[2]) < 20*M_PI/180.0){
                    found_solution = true;
                    cout <<"Solution found with "<< segments.size()<<" segments visited!"<<endl;
                    goal_seg= path_seg_tmp;
    				break;
            	}
            }
            if (ros::Time::now().toSec() - start_time > durationIn){
            	cout << "No solution found after timing out " << endl;
            	break;
            }
            for(vector<double>::iterator iter_steer= steerings_.begin(); iter_steer != steerings_.end(); iter_steer++) {
            	for (vector<double>::iterator iter_step = steps_.begin(); iter_step != steps_.end(); iter_step ++){
				bool success;
                	PathSegment path_seg_next = steer(path_seg_tmp, *iter_step, *iter_steer, success);
                	if (!success) {
                		continue;
                	}
                	DiscreteState discrete_state_next = discretize(path_seg_next.state);
                	if (closed_set.count(discrete_state_next)) {
                		continue;
                	}
                	double tentative_cost = path_seg_next.cost + path_seg_next.heuristic;

                	path_seg_next.index = segments.size();
                	segments.push_back(path_seg_next);
                	priority_queue.push(QPair(path_seg_next.cost + path_seg_next.heuristic, path_seg_next.index));
                	open_set.insert(discretize(path_seg_next.state));
            	}
            }
            closed_set.insert(discretize(path_seg_tmp.state));
        }
        if (found_solution) {
            int index = goal_seg.index;
            while (index >= 0) {
            	solutionPathOut.push_back(segments[index].state);
            	shiftOut.push_back(segments[index].shift);
            	index = segments[index].prev_index;
            }
            reverse(solutionPathOut.begin(), solutionPathOut.end());
            reverse(shiftOut.begin(), shiftOut.end());
            return true;
        }
        else
        	return false;
    }

    PathSegment
	HybridAStar::
	steer(const PathSegment& pathSegIn, double stepIn, double thetaIn, bool& success) {
        const State& state_start = pathSegIn.state;
        double theta_end = state_start[2] + stepIn/1.5*tan(thetaIn);
        PathSegment path_seg_next;
        path_seg_next.state.resize(3);
        path_seg_next.state[0] = state_start[0] + stepIn * cos(theta_end);;
        path_seg_next.state[1] = state_start[1] + stepIn * sin(theta_end);;
        path_seg_next.state[2] = boundAngle(theta_end);

        //double move_cost = step_length_;
        double move_cost = (metric_checker_->getMetric(path_seg_next.state) + metric_checker_->getMetric(pathSegIn.state))/2;
        move_cost += stepIn > 0 ? fabs(stepIn)*step_cost_forward_: fabs(stepIn)*step_cost_backward_;
        //cout <<"Move cost with steer angle: "<< move_cost<<thetaIn <<endl;
        double steer_cost = fabs(thetaIn) * steering_cost_;

		success = (metric_checker_->getMetric(path_seg_next.state) < 1e3);
		//if(angleDist(angleToGoal(path_seg_next.state), path_seg_next.state[2]) > M_PI / 180.0 * 90.0) {
		//	success = false;
		//}
		path_seg_next.discount = pathSegIn.discount * cost_discount_;
		path_seg_next.cost = pathSegIn.cost + (move_cost + steer_cost) * path_seg_next.discount;
		path_seg_next.heuristic = heuristic(path_seg_next.state) * path_seg_next.discount;
		path_seg_next.prev_index = pathSegIn.index;
		path_seg_next.shift = stepIn > 0 ? 1 : -1;
        if (false){
        	ROS_INFO("Steer angle: %f", thetaIn);
        	ROS_INFO("Initial State: %f, %f, %f", state_start[0], state_start[1], state_start[2]);
        	ROS_INFO("Metric Cost: %f, steer_cost %f", move_cost, steer_cost);
        	ROS_INFO("Updated State: %f, %f, %f", path_seg_next.state[0], path_seg_next.state[1], path_seg_next.state[2]);
        }
        return path_seg_next;
    }

    double
	HybridAStar::
	angleToGoal(const State& stateIn) {
        return (atan2(goal_[1] - stateIn[1], goal_[0] - stateIn[0]));
    }

    double
	HybridAStar::
	distToGoal(const State& stateIn) {
        return sqrt(SQR(goal_[0]-stateIn[0]) + SQR(goal_[1]-stateIn[1]));
    }

    //TODO: Design better heuristic function
    double
	HybridAStar::
	heuristic(const State& stateIn) {
    	//return distToGoal(stateIn);
        double num_step = distToGoal(stateIn) / double(step_length_);
        double dist_heuristic = (1 - pow(cost_discount_, num_step)) / (1 - cost_discount_) * GOAL_HEURISTIC ;
        double nonholonomic_heuristic = angleDist(stateIn[2], goal_[2]);
        return dist_heuristic + steering_cost_ * nonholonomic_heuristic;
    	//return 0.0;
    }

    bool
	HybridAStar::
	isGoalReached(const State& stateIn) {
        return (distToGoal(stateIn) < GOAL_TOLERANCE && angleDist(stateIn[2],goal_[2]) < 60/180*M_PI);
        //return (distToGoal(stateIn) < GOAL_TOLERANCE);
    }

    DiscreteState
	HybridAStar::
	discretize(const State& stateIn) {
        DiscreteState disctete_state(3);
		double dd = step_length_ * discretize_ratio_;
		disctete_state[0] = int((stateIn[0] / dd));
		disctete_state[1] = int((stateIn[1] / dd));
		disctete_state[2] = int(((stateIn[2] + M_PI) / (steering_resolution_*0.2)));
        return disctete_state;
    }

    void
	HybridAStar::
	resetMetricMap(const MetricMapPtr& metricMapIn){
    	metric_checker_->updateMetricMap(metricMapIn);
    }
}

