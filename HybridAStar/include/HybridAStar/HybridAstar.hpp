/*
 * HybridAstar.hpp
 *
 *  Created on: 24 Mar, 2015
 *      Author: liuwlz
 */

#ifndef INCLUDE_HYBRIDASTAR_HYBRIDASTAR_HPP_
#define INCLUDE_HYBRIDASTAR_HYBRIDASTAR_HPP_


#include <vector>
#include <queue>
#include <set>

#include <boost/shared_ptr.hpp>
#include <nav_msgs/Path.h>

#include <HybridAStar/MetricChecker.hpp>

#include <Metric_Map/MetricMapMsg.h>

#define GOAL_TOLERANCE 0.5
#define GOAL_HEURISTIC 2

using namespace boost;
using namespace std;

namespace MotionPlan {

	typedef vector<double> State;
    typedef vector<int> DiscreteState;
    typedef pair<double, int> QPair;
	typedef shared_ptr<Metric_Map::MetricMapMsg> MetricMapPtr;

	struct PathSegment{
        State state;
        double cost, heuristic;
        int index;
        int prev_index;
		double discount;
        int shift;
	};

	class HybridAStar {
	public:
		HybridAStar();
		virtual ~HybridAStar();

		void setupPlanner();
		void resetPlanner();
		void resetMetricMap(const MetricMapPtr& metricMapIn);
		void resetStart(const State& rootIn);
		void resetGoal(const State& goalIn);

        vector<PathSegment> segments;
		nav_msgs::Path getSolutionPath()const;
		bool searchPath(double durationIn, vector<State>& solutionPathOut, vector<int>& shiftOut);

	private:

		vector<double> steerings_, steps_;
		vector<double> goal_, start_;
		shared_ptr<MetricChecker> metric_checker_;

		double steering_limit_, steering_resolution_, steering_cost_;
		double step_length_, discretize_ratio_, step_cost_forward_, step_cost_backward_;
		double cost_discount_;

		PathSegment steer(const PathSegment& pathSegIn, double stepIn, double thetaIn, bool& success);
	    DiscreteState discretize(const State& stateIn);
	    double heuristic(const State& stateIn);
	    double angleToGoal(const State& stateIn);
	    double distToGoal(const State& stateIn);
	    bool isGoalReached(const State& stateIn);
	};

	inline double SQR(double x) { return x*x;}

	inline double boundAngle(double angle) {
        while(angle < -M_PI) angle += 2*M_PI;
        while(angle > M_PI) angle -= 2*M_PI;
        return angle;
    }

    inline double angleDist(double angleA, double angleB) {
        double angle_dist = fabs(boundAngle(boundAngle(angleA)-boundAngle(angleB)));
        return angle_dist;
    }



}  // namespace MotionPlan




#endif /* INCLUDE_HYBRIDASTAR_HYBRIDASTAR_HPP_ */
