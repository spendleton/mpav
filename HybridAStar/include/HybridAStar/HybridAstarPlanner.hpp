/*
 * HybridAstarPlanner.hpp
 *
 *  Created on: 24 Mar, 2015
 *      Author: liuwlz
 */

#ifndef INCLUDE_HYBRIDASTAR_HYBRIDASTARPLANNER_HPP_
#define INCLUDE_HYBRIDASTAR_HYBRIDASTARPLANNER_HPP_

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <visualization_msgs/Marker.h>

#include <HybridAStar/HybridAstar.hpp>

namespace MotionPlan {

	class HybridAStarPlanner {
	public:
		HybridAStarPlanner();
		~HybridAStarPlanner();

	private:
		ros::NodeHandle nh_, pri_nh_;

		ros::Subscriber metric_map_sub_, rviz_goal_sub_;
		ros::Publisher solution_path_pub_, search_result_pub_;
		ros::Timer plan_timer_;

		nav_msgs::Path solution_path_;
		shared_ptr<HybridAStar> hybrid_astar_;
		Metric_Map::MetricMapMsgPtr metric_map_;

		tf::TransformListener tf_;

		vector<double> vehicle_pose_;
		State start_, goal_;
		double planning_duration_;
		bool is_goal_set_, is_metric_map_set_;

		string base_frame_, global_frame_;

		void analyzeSolutionPath(vector<State>& solutionPathIn, vector<int>& shiftCtrIn);
		void setPlanningGoal(const vector<double>& goalIn);
		bool getVehiclePose();
		void planTimer(const ros::TimerEvent& e);
		void metricMapCallBack(const Metric_Map::MetricMapMsgPtr& metricMapIn);
		void rvizGoalCallBack(const geometry_msgs::PoseStamped& goalIn);
		double getYawofPose(const geometry_msgs::Pose& poseIn);
		bool transformToGlobalFrame(const geometry_msgs::Pose& poseIn, geometry_msgs::Pose& poseOut);
		void visualizeSearchResult();
	};

}  // namespace MotionPlan



#endif /* INCLUDE_HYBRIDASTAR_HYBRIDASTARPLANNER_HPP_ */
