/*
 * NormPlan.hpp
 *
 *  Created on: Nov 29, 2013
 *      Author: liuwlz
 */

#ifndef NORMPLAN_HPP_
#define NORMPLAN_HPP_

#include <Automoton_Control/Navigation/NaviSM.hpp>

namespace BehaviorPlan{

	struct NormPlanSM : sc::state<NormPlanSM,MovingSM>{
		typedef mpll::list<
				sc::custom_reaction<Ev_ObstAvoid>,
				sc::custom_reaction<Ev_ReachDest>,
				sc::custom_reaction<Ev_Parking>,
				sc::custom_reaction<Ev_UTurn>
				> reactions;
		typedef sc::state<NormPlanSM, MovingSM> Base;
		NormPlanSM(my_context ctx) : Base(ctx){
			ROS_WARN("Enter Normal Plan");
			context<NaviSM>().sm_visualizer_->sm_status_marker_->text = "NormalPlan";
			context<NaviSM>().stanley_steer_ctr_->dist_to_goal_ = DBL_MAX;
			context<NaviSM>().stanley_steer_ctr_->steer_control_timer_.stop();
			context<NaviSM>().replan_pp_steering_ctr_->dist_to_goal_ = DBL_MAX;
			context<NaviSM>().replan_pp_steering_ctr_->steer_control_timer_.stop();
			context<NaviSM>().pp_steering_ctr_->steer_control_timer_.start();
			context<NaviSM>().simulate_speed_ctr_->speed_control_timer_.start();
			context<NaviSM>().simulate_speed_ctr_->SetStatus(0);
			context<NaviSM>().obst_replan_trigger_->trigger_reason_timer_.start();
			context<NaviSM>().reedshepp_planner_->plan_timer_.stop();
			context<NaviSM>().dubins_planner_->plan_timer_.stop();
		}

		virtual ~NormPlanSM(){

		}

		sc::result react(const Ev_ObstAvoid &){
			return transit<ObstAvoidSM>();
		}

		sc::result react(const Ev_Parking &){
			return transit<ParkingSM>();
		}

		sc::result react(const Ev_UTurn &){
			return transit<UTurnSM>();
		}

		sc::result react(const Ev_ReachDest& ){
			return forward_event();
		}
	};
}

#endif /* NORMPLAN_HPP_ */
