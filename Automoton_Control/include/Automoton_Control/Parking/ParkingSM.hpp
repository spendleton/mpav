/*
 * ParkingSM.hpp
 *
 *  Created on: 21 Jul, 2014
 *      Author: liuwlz
 */

#ifndef PARKINGSM_HPP_
#define PARKINGSM_HPP_

#include <Automoton_Control/Navigation/NaviSM.hpp>

namespace BehaviorPlan {
	struct ParkingSM:sc::state<ParkingSM,MovingSM>{
		typedef mpll::list<
					sc::custom_reaction<Ev_ParkingEnd>
				>reactions;
		typedef sc::state<ParkingSM,MovingSM> base;
		ParkingSM(my_context ctx):base(ctx){
			ROS_INFO_ONCE("Enter Parking");
		}

		virtual ~ParkingSM(){
			ROS_INFO_ONCE("Exit Parking");
		}

		sc::result react(const Ev_ParkingEnd&){
			return transit<NormPlanSM>();
		}
	};
}  // namespace BehaviorPlan

#endif /* PARKINGSM_HPP_ */
