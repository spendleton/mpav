/*
 * UTurnSM.hpp
 *
 *  Created on: 21 Jul, 2014
 *      Author: liuwlz
 */

#ifndef UTURNSM_HPP_
#define UTURNSM_HPP_

#include <Automoton_Control/Navigation/NaviSM.hpp>

namespace BehaviorPlan {

	struct UTurnPlan;
	struct HeadUturn;

	struct UTurnSM:sc::state<UTurnSM, MovingSM, UTurnPlan>{
		typedef mpll::list<
				sc::custom_reaction<Ev_ReachSubGoal>
		> reactions;
		typedef sc::state<UTurnSM, MovingSM, UTurnPlan> Base;

		UTurnSM(my_context ctx):Base(ctx){
			ROS_WARN("Enter UTurn");
			context<NaviSM>().sm_visualizer_->sm_status_marker_->text = "UTurn";
			context<NaviSM>().pp_steering_ctr_->steer_control_timer_.stop();
			context<NaviSM>().uturn_goal_->setGoalType(context<NaviSM>().uturn_type_);
			context<NaviSM>().uturn_goal_->makeUTurnGoal();
		}

		virtual ~UTurnSM(){
			context<NaviSM>().reedshepp_planner_->resetPlannerStatus();
			context<NaviSM>().pp_steering_ctr_->resetControlLoop();
		}

		sc::result react(const Ev_ReachSubGoal){
			return transit<NormPlanSM>();
		}
	};

	struct UTurnPlan:sc::state<UTurnPlan,UTurnSM>{
		typedef mpl::list<
				sc::custom_reaction<Ev_UTurnPathFound>
				> reactions;

		typedef sc::state<UTurnPlan, UTurnSM> Base;

		UTurnPlan(my_context ctx):Base(ctx){
			ROS_WARN("UTurn Planning");
			context<NaviSM>().simulate_speed_ctr_->SetStatus(3);
			context<NaviSM>().stanley_steer_ctr_->steer_control_timer_.stop();
			context<NaviSM>().reedshepp_planner_->updateGoal(context<NaviSM>().uturn_goal_->uturn_goal_);
			context<NaviSM>().reedshepp_planner_->plan_timer_.start();
		}

		virtual ~UTurnPlan(){

		}

		sc::result react(const Ev_UTurnPathFound){
			return transit<HeadUturn>();
		}
	};

	struct HeadUturn:sc::state<HeadUturn,UTurnSM>{
		typedef mpl::list<
				sc::custom_reaction <Ev_UTurnPathDanger>,
				sc::custom_reaction<Ev_ReachSubGoal>
				> reactions;
		typedef sc::state<HeadUturn, UTurnSM> Base;
		HeadUturn(my_context ctx):Base(ctx){
			ROS_WARN("Head UTurn");
			context<NaviSM>().stanley_steer_ctr_->initTrackingPath(context<NaviSM>().reedshepp_planner_->getSolutionPath());
			context<NaviSM>().stanley_steer_ctr_->steer_control_timer_.start();
			context<NaviSM>().simulate_speed_ctr_->SetStatus(1);
		}

		virtual ~HeadUturn(){
			context<NaviSM>().stanley_steer_ctr_->resetControlLoop();
		}

		sc::result react(const Ev_UTurnPathDanger){
			return transit<UTurnPlan>();
		}

		sc::result react(const Ev_ReachSubGoal){
			return forward_event();
		}
	};
}  // namespace BehaviorPlan


#endif /* UTURNSM_HPP_ */
