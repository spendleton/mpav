/*
 * NaviSM.hpp
 *
 *  Created on: Nov 29, 2013
 *      Author: liuwlz
 */

#ifndef NAVISM_HPP_
#define NAVISM_HPP_

#include <vector>
#include <string>

#include <boost/statechart/state.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/event.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/mpl/list.hpp>

#include <Automoton_Control/Navigation/NaviEvents.hpp>
#include <Automoton_Control/SM_Msg.h>
#include <Automoton_Control/SMVisualizer.hpp>

#include <RoutePlanner/StationRouting.hpp>
#include <RoutePlanner/StationIntegration.hpp>
#include <Metric_Map/Extend_Metric_Map.hpp>
#include <Metric_Map/Prior_Metric_Map.hpp>
#include <Steering_Control/PP_Steering_Control.hpp>
#include <Steering_Control/Stanley_Steering_Control.hpp>
#include <Speed_Control/Speed_Control.hpp>
#include <Speed_Control/Simulate_Speed_Advisor.hpp>
#include <ReplanTrigger/ObstReact_Replan_Trigger.hpp>
#include <OMPLRRTS/RRTS_Planner.hpp>
#include <Goal_Generator/Obst_Avoid_Goal.hpp>
#include <Goal_Generator/UTurn_Goal.hpp>
#include <Speed_Smoother/Speed_Smoother.hpp>

namespace sc = boost::statechart;
namespace mpll = boost::mpl;
using namespace std;

namespace BehaviorPlan{
	/**
	 * Forward definition for states and state machine
	 */
	struct IdleCheckSM;
	struct MissionWaitingSM;
	struct InitPerceptionSM;
	struct MovingSM;
	struct NormPlanSM;
	struct ObstAvoidSM;
	struct ParkingSM;
	struct UTurnSM;
	struct NaviSM;

	enum StateID {IdleCheck, Mission, Perception, Moving, Normal, ObstAvoid, Parking, UTurn};
}

#endif /* NAVISM_HPP_ */
