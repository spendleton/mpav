/* Copyright (C) <2014>  <Wei Liu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published bythe Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   
 *
 * SMVisualizer.hpp
 *
 *  Created on: 25 Sep, 2014
 *      Author: liuwlz
 */

#ifndef SMVISUALIZER_HPP_
#define SMVISUALIZER_HPP_

#include <ros/ros.h>

#include <tf/tf.h>
#include <tf/message_filter.h>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

using namespace std;
using namespace boost;


namespace BehaviorPlan {

	class SMVisualizer {
	public:
		SMVisualizer();
		virtual ~SMVisualizer();

		visualization_msgs::MarkerPtr vehicle_marker_;
		visualization_msgs::MarkerPtr sm_status_marker_, speed_status_marker_;
		visualization_msgs::MarkerArrayPtr status_markerarray_;
	private:
		ros::NodeHandle nh_, pri_nh_;
		ros::Timer visualize_timer_;
		ros::Publisher status_markerarray_pub_;

		string global_frame_, base_frame_;

		void visualizeSMTimer(const ros::TimerEvent& e);
		void makeMarker(visualization_msgs::MarkerPtr& markerIn, int markerType);
	};


}  // namespace BehaviorPlan



#endif /* SMVISUALIZER_HPP_ */
