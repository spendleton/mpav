/*
 * ObstAvoidSM.hpp
 *
 *  Created on: Nov 29, 2013
 *      Author: liuwlz
 */

#ifndef OBSTAVOIDSM_HPP_
#define OBSTAVOIDSM_HPP_

#include <Automoton_Control/Navigation/NaviSM.hpp>

namespace BehaviorPlan{
	struct HeadGoal;
	struct SlowMove;

	struct ObstAvoidSM:sc::state<ObstAvoidSM, MovingSM, SlowMove>{
		typedef mpll::list<
				sc::custom_reaction<Ev_ReachSubGoal>,
				sc::custom_reaction<Ev_ReachDest>
				> reactions;
		typedef sc::state<ObstAvoidSM, MovingSM, SlowMove> Base;

		ObstAvoidSM(my_context ctx):Base(ctx){
			ROS_WARN("Enter Obstacle Avoid");
			context<NaviSM>().sm_visualizer_->sm_status_marker_->text = "ObstacleAvoid";
			context<NaviSM>().obst_avoid_goal_->setReplanHorizon(2 * context<NaviSM>().obst_replan_trigger_->getTriggerDist());
			context<NaviSM>().obst_avoid_goal_->makeSubGoal();
			if (context<NaviSM>().steering_type_ != 1)
				context<NaviSM>().replan_pp_steering_ctr_->initTrackingPath(context<NaviSM>().modifed_path_);
		}

		virtual ~ObstAvoidSM(){
			context<NaviSM>().dubins_planner_->resetPlannerStatus();
		}

		sc::result react(const Ev_ReachDest){
			return forward_event();
		}

		sc::result react(const Ev_ReachSubGoal){
			return transit<NormPlanSM>();
		}
	};

	struct SlowMove:sc::state<SlowMove, ObstAvoidSM>{
		typedef mpl::list<
				sc::custom_reaction<Ev_ReplanPathFound>,
				sc::custom_reaction<Ev_ObstAvoid>,
				sc::custom_reaction<Ev_ReachSubGoal>,
				sc::custom_reaction<Ev_ReachDest>
				> reactions;

		typedef sc::state<SlowMove, ObstAvoidSM> Base;

		SlowMove(my_context ctx):Base(ctx){
			ROS_WARN("Enter SlowMove");
			context<NaviSM>().sm_visualizer_->sm_status_marker_->text = "SlowMove";
			if (context<NaviSM>().steering_type_ == 1){
				context<NaviSM>().pp_steering_ctr_->steer_control_timer_.start();
				context<NaviSM>().stanley_steer_ctr_->steer_control_timer_.stop();
			}
			else{
				context<NaviSM>().pp_steering_ctr_->steer_control_timer_.stop();
				context<NaviSM>().replan_pp_steering_ctr_->steer_control_timer_.start();
			}
			context<NaviSM>().simulate_speed_ctr_->SetStatus(1);
			context<NaviSM>().dubins_planner_->updateGoal(context<NaviSM>().obst_avoid_goal_->sub_goal_);
			context<NaviSM>().dubins_planner_->plan_timer_.start();
		}

		virtual ~SlowMove(){

		}

		sc::result react(const Ev_ReplanPathFound&){
			return transit<HeadGoal>();
		}

		sc::result react(const Ev_ReachSubGoal&){
			return forward_event();
		}

		sc::result react(const Ev_ObstAvoid&){
			return discard_event();
		}

		sc::result react(const Ev_ReachDest){
			return forward_event();
		}
	};

	struct HeadGoal:sc::state<HeadGoal, ObstAvoidSM>{
		typedef mpl::list<
				sc::custom_reaction <Ev_ReplanPathDanger>,
				sc::custom_reaction <Ev_ReachSubGoal>,
				sc::custom_reaction<Ev_ReachDest>
			> reactions;

		typedef sc::state<HeadGoal, ObstAvoidSM> Base;
		HeadGoal(my_context ctx):Base(ctx){
			ROS_WARN("Enter HeadGoal");
			context<NaviSM>().sm_visualizer_->sm_status_marker_->text = "HeadGoal";
			context<NaviSM>().simulate_speed_ctr_->SetStatus(2);
			if (context<NaviSM>().steering_type_ == 1){

				context<NaviSM>().pp_steering_ctr_->combineTrackingPath(
						context<NaviSM>().dubins_planner_->getSolutionPath(),context<NaviSM>().obst_avoid_goal_->remain_path_);
				context<NaviSM>().modifed_path_ = context<NaviSM>().pp_steering_ctr_->combineModifiedPath(
						context<NaviSM>().dubins_planner_->getSolutionPath(),context<NaviSM>().obst_avoid_goal_->remain_path_);

				context<NaviSM>().stanley_steer_ctr_->initTrackingPath(context<NaviSM>().dubins_planner_->getSolutionPath());
				context<NaviSM>().pp_steering_ctr_->steer_control_timer_.stop();
				context<NaviSM>().stanley_steer_ctr_->steer_control_timer_.start();
			}
			else{

				context<NaviSM>().modifed_path_ = context<NaviSM>().replan_pp_steering_ctr_->combineModifiedPath(
						context<NaviSM>().dubins_planner_->getSolutionPath(),context<NaviSM>().obst_avoid_goal_->remain_path_);

				context<NaviSM>().replan_pp_steering_ctr_->initTrackingPath( context<NaviSM>().dubins_planner_->getSolutionPath());
				context<NaviSM>().replan_pp_steering_ctr_->steer_control_timer_.start();
			}
		}

		virtual ~HeadGoal(){
			if (context<NaviSM>().steering_type_ == 1){
				context<NaviSM>().stanley_steer_ctr_->resetControlLoop();;
			}
			else{
				context<NaviSM>().replan_pp_steering_ctr_->resetControlLoop();
			}
		}

		sc::result react(const Ev_ReachSubGoal &){
			return forward_event();
		}

		sc::result react(const Ev_ReplanPathDanger &){
			return transit<SlowMove>();
		}

		sc::result react(const Ev_ReachDest){
			return forward_event();
		}
	};
}

#endif /* OBSTAVOIDSM_HPP_ */
