/*
 * NavigationSMNode.cpp
 *
 *  Created on: 11 Aug, 2014
 *      Author: liuwlz
 */

#include <Automoton_Control/NavigationSMNode.hpp>

namespace BehaviorPlan{
	NaviStateMachine::
	NaviStateMachine():
	pri_nh_("~"),
	init_replan_timer_(false){

		navi_sm_ = shared_ptr<NaviSM>(new NaviSM());

		pri_nh_.param("obst_replan",obst_replan_,true);
		pri_nh_.param("uturn_type",navi_sm_->uturn_type_,-2);
		pri_nh_.param("planner_type",navi_sm_->planner_type_,2);
		pri_nh_.param("metric_map_type",navi_sm_->metric_map_type_,1);
		pri_nh_.param("steering_type",navi_sm_->steering_type_,1);
		pri_nh_.param("goal_reach_dist",goal_reach_dist_,3.0);
		pri_nh_.param("replan_duration_tolerance",replan_duration_tolerance_,3.0);

		navi_sm_->sm_visualizer_ = shared_ptr<SMVisualizer>(new SMVisualizer());
		navi_sm_->station_integration_ = shared_ptr<StationIntegration>(new StationIntegration());
		navi_sm_->pp_steering_ctr_ = shared_ptr<PPSteeringCtr>(new PPSteeringCtr());
		navi_sm_->replan_pp_steering_ctr_ = shared_ptr<PPSteeringCtr>(new PPSteeringCtr());
		navi_sm_->stanley_steer_ctr_ = shared_ptr<StanleySteeringCtr>(new StanleySteeringCtr());
		navi_sm_->simulate_speed_ctr_ = shared_ptr<SpeedControl<SimulateSpeedAdvisor> >(new SpeedControl<SimulateSpeedAdvisor>());
		navi_sm_->obst_replan_trigger_ = shared_ptr<ObstReactReplanTrigger>(new ObstReactReplanTrigger());
		navi_sm_->reedshepp_planner_ =shared_ptr<RRTSPlanner>(new RRTSPlanner(2.0));
		navi_sm_->dubins_planner_ = shared_ptr<RRTSPlanner>(new RRTSPlanner(3.0));
		navi_sm_->obst_avoid_goal_ = shared_ptr<ObstAvoidGoal>(new ObstAvoidGoal());
		navi_sm_->uturn_goal_ = shared_ptr<UTurnGoal>(new UTurnGoal());

		if (navi_sm_->metric_map_type_ == 1)
			navi_sm_->prior_metric_map_ = shared_ptr<PriorMetricMap>(new PriorMetricMap());
		else
			navi_sm_->extend_metric_map_ = shared_ptr<ExtendMetricMap>(new ExtendMetricMap());

		status_check_timer_ = nh_.createTimer(ros::Duration(0.05), &NaviStateMachine::statusCheckTimer,this);
		reference_path_sub_ = nh_.subscribe("route_plan",1,&NaviStateMachine::referencePathCallBack,this);
		uturn_service_ = nh_.advertiseService("uturn",&NaviStateMachine::uTurnServiceCallBack,this);

		navi_sm_->initiate();
	}

	NaviStateMachine::
	~NaviStateMachine(){

	}

	void
	NaviStateMachine::
	referencePathCallBack(const nav_msgs::Path& pathIn){
		if (navi_sm_->reference_path_.poses.size() == pathIn.poses.size()){
			return;
		}
		ROS_INFO("Receive new path");
		if (navi_sm_->state_downcast<const MissionWaitingSM*>() != 0){
			navi_sm_->reference_path_ = pathIn;
			navi_sm_->modifed_path_ = pathIn;
			navi_sm_->process_event(Ev_MissionRecived());
		}
	}

	void
	NaviStateMachine::
	statusCheckTimer(const ros::TimerEvent& e){

		checkReplanTimeOut();

		if (navi_sm_->pp_steering_ctr_->dist_to_goal_ < goal_reach_dist_)
			navi_sm_->process_event(Ev_ReachDest());

		if (navi_sm_->stanley_steer_ctr_->dist_to_goal_ < 1.0)
			navi_sm_->process_event(Ev_ReachSubGoal());

		if (navi_sm_->replan_pp_steering_ctr_->dist_to_goal_ < 1.5)
			navi_sm_->process_event(Ev_ReachSubGoal());

		if (navi_sm_->obst_replan_trigger_->trigger_status_ == 0 && obst_replan_)
			navi_sm_->process_event(Ev_ObstAvoid());

		if (navi_sm_->reedshepp_planner_->plan_status_ == 1)
			navi_sm_->process_event(Ev_UTurnPathFound());

		if (navi_sm_->reedshepp_planner_->plan_status_ ==2 || navi_sm_->reedshepp_planner_->plan_status_==0)
			navi_sm_->process_event(Ev_UTurnPathDanger());

		if (navi_sm_->stanley_steer_ctr_->over_tracking_error_)
			navi_sm_->process_event(Ev_UTurnPathDanger());

		if (navi_sm_->dubins_planner_->plan_status_ == 1)
			navi_sm_->process_event(Ev_ReplanPathFound());

		if (navi_sm_->dubins_planner_->plan_status_ ==2 || navi_sm_->dubins_planner_->plan_status_==0)
			navi_sm_->process_event(Ev_ReplanPathDanger());
	}

	void
	NaviStateMachine::
	checkReplanTimeOut(){
		//This is a timeout function of replanning, which aims at handing the infeasible goal issue.
		double replan_duration = 0.0;
		if (navi_sm_->state_downcast<const SlowMove*>() != 0){
			if (!init_replan_timer_){
				replan_time_ = ros::Time::now();
				init_replan_timer_ = true;
			}
			else{
				replan_duration = (ros::Time::now() - replan_time_).toSec();
			}
			if (replan_duration > replan_duration_tolerance_){
				ROS_WARN("Replan Timeout with duration %f, try to reverse to Normal Plan", replan_duration);
				init_replan_timer_ = false;
				if (navi_sm_->state_downcast<const SlowMove*>() != 0)
					navi_sm_->process_event(Ev_ReachSubGoal());
			}
		}
		else{
			init_replan_timer_ = false;
		}
	}

	bool
	NaviStateMachine::
	uTurnServiceCallBack(Automoton_Control::UTurnService::Request& reqIn,
			Automoton_Control::UTurnService::Response& resOut){
		if (navi_sm_->state_downcast<const NormPlanSM*>() != 0){
			navi_sm_->process_event(Ev_UTurn());
			resOut.res = true;
			return true;
		}
		else{
			ROS_ERROR("Vehicle not in NormalPlan Mode");
			resOut.res = false;
			return false;
		}
	}
}
