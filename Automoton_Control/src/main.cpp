/*
 * main.cpp
 *
 *  Created on: Nov 19, 2013
 *      Author: liuwlz
 */

#include <Automoton_Control/NavigationSMNode.hpp>

int main(int argc, char**argv){
	ros::init(argc, argv, "Navigation");
	BehaviorPlan::NaviStateMachine	navigation;
	ros::spin();
	return 1;
}
