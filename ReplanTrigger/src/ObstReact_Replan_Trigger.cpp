/*
 * ObstReact_Replan_Trigger.cpp
 *
 *  Created on: 10 Aug, 2014
 *      Author: liuwlz
 */


#include <ReplanTrigger/ObstReact_Replan_Trigger.hpp>

namespace Perception{
	ObstReactReplanTrigger::
	ObstReactReplanTrigger():
	trigger_duration_(0.0),
	fused_scan_init_(false),
	simulate_path_init_(false),
	initial_trigger_init_(false),
	trigger_bound_dist_(6.0){
		pri_nh_.param("trigger_bound_time",trigger_bound_time_,0.5);
		pri_nh_.param("trigger_bound_width",trigger_bound_width_,1.25);
		pri_nh_.param("trigger_min_dist",trigger_min_dist_,3.0);
		pri_nh_.param("trigger_max_dist",trigger_max_dist_,9.0);
		pri_nh_.param("early_trigger_ratio",early_trigger_ratio_,0.8);
		pri_nh_.param("max_speed",trigger_max_vel_,2.0);

		fused_scan_ = shared_ptr<sensor_msgs::PointCloud>(new sensor_msgs::PointCloud());
		simulate_path_ = shared_ptr<nav_msgs::Path>(new nav_msgs::Path());

		fused_scan_sub_ = nh_.subscribe("fused_scan_pts",1,&ObstReactReplanTrigger::fusedScanCallBack,this);
		simulate_path_sub_ = nh_.subscribe("simulate_path",1,&ObstReactReplanTrigger::simulatePathCallBack,this);
		cmd_vel_sub_ = nh_.subscribe("cmd_vel",1,&ObstReactReplanTrigger::cmdVelCallBack,this);
		trigger_reason_timer_ = nh_.createTimer(ros::Duration(0.1),&ObstReactReplanTrigger::triggerReasonTimer,this);
	}

	ObstReactReplanTrigger::
	~ObstReactReplanTrigger(){

	}

	void
	ObstReactReplanTrigger::
	fusedScanCallBack(const sensor_msgs::PointCloudPtr scanIn){
		scanIn->header.stamp = ros::Time();
		fused_scan_->header.stamp = ros::Time();
		try{
			tf_.waitForTransform(global_frame_,base_frame_,ros::Time(0),ros::Duration(0.01));
			tf_.transformPointCloud(global_frame_,*scanIn, *fused_scan_);
		}
		catch (tf::TransformException &ex){
			ROS_WARN("Failure %s\n", ex.what());
			return;
		}
		fused_scan_->header.stamp = ros::Time::now();
		fused_scan_init_ = true;
	}

	void
	ObstReactReplanTrigger::
	simulatePathCallBack(const nav_msgs::PathPtr pathIn){
		simulate_path_ = pathIn;
		simulate_path_init_ = true;
	}

	void
	ObstReactReplanTrigger::
	cmdVelCallBack(const geometry_msgs::TwistConstPtr velIn){
		double vehicle_vel = velIn->linear.x;
		double max_dec = trigger_max_vel_*trigger_max_vel_/(2*(trigger_max_dist_- trigger_min_dist_)) * early_trigger_ratio_;
		trigger_bound_dist_ = trigger_min_dist_ + vehicle_vel*vehicle_vel/(2 * max_dec);
		trigger_bound_dist_ = trigger_bound_dist_ > trigger_max_dist_ ? trigger_max_dist_ : trigger_bound_dist_;
		if(TriggerDebug)
			ROS_INFO("Current max_dec: %f, vel: %f, Trigger_bound_dist: %f", max_dec, vehicle_vel, trigger_bound_dist_);
	}

#define SQ(x) ((x)*(x))
	void
	ObstReactReplanTrigger::
	triggerReasonTimer(const ros::TimerEvent& e){
		double path_scan_dist;
		if (!fused_scan_init_ || !simulate_path_init_)
			return;
		double longitude_dist = 0;
		for (vector<geometry_msgs::PoseStamped>::iterator iter_path = simulate_path_->poses.begin();
				iter_path != simulate_path_->poses.end();
				iter_path ++){
			longitude_dist += sqrt(SQ(iter_path->pose.position.x - (iter_path+1)->pose.position.x)
					+ SQ(iter_path->pose.position.y - (iter_path+1)->pose.position.y));
			for (vector<geometry_msgs::Point32>::iterator iter_scan = fused_scan_->points.begin();
					iter_scan != fused_scan_->points.end()-1;
					iter_scan++){
				path_scan_dist = distBetweenPosewithPoint(*iter_path, *iter_scan);
				if (path_scan_dist <= trigger_bound_width_ && longitude_dist < trigger_bound_dist_){
					if (!initial_trigger_init_){
						initial_trigger_time_ = ros::Time::now();
						initial_trigger_init_ = true;
					}
					else
						trigger_duration_ = (ros::Time::now()-initial_trigger_time_).toSec();
					if (trigger_duration_>trigger_bound_time_){
						trigger_status_ = Obst;
						trigger_dist_ = longitude_dist < trigger_min_dist_ ? trigger_min_dist_ : longitude_dist;
					}
					return;
				}
			}
		}
		initial_trigger_init_ = false;
		trigger_duration_ = 0.0;
		trigger_status_ = Disable;
		ROS_INFO_ONCE("Trigger Reason Spinning");
	}
}  // namespace Perception
