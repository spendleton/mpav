/*
 * Replan_Trigger.cpp
 *
 *  Created on: 10 Aug, 2014
 *      Author: liuwlz
 */

#include <ReplanTrigger/Replan_Trigger.hpp>

namespace Perception{
	ReplanTrigger::
	ReplanTrigger():
	pri_nh_("~"),
	trigger_status_(Disable){
		pri_nh_.param("global_frame",global_frame_,string("map"));
		pri_nh_.param("base_frame",base_frame_,string("base_link"));
	}

	ReplanTrigger::
	~ReplanTrigger(){

	}

	double
	ReplanTrigger::
	distBetweenPosewithPoint(const geometry_msgs::PoseStamped& poseIn, const geometry_msgs::Point32& pointIn){
		double x_diff = pointIn.x - poseIn.pose.position.x;
		double y_diff = pointIn.y - poseIn.pose.position.y;
		return sqrt(x_diff*x_diff + y_diff*y_diff);
	}



}  // namespace Perception
