/*
 * ObstReact_Replan_Trigger.hpp
 *
 *  Created on: 10 Aug, 2014
 *      Author: liuwlz
 */

#ifndef OBSTREACT_REPLAN_TRIGGER_HPP_
#define OBSTREACT_REPLAN_TRIGGER_HPP_

#include <ReplanTrigger/Replan_Trigger.hpp>

#define TriggerDebug false

namespace Perception{
	class ObstReactReplanTrigger: public ReplanTrigger{
	public:
		ObstReactReplanTrigger();
		virtual ~ObstReactReplanTrigger();

		ros::Timer trigger_reason_timer_;
		double getTriggerDist()const{return trigger_dist_;};

	private:

		ros::Subscriber simulate_path_sub_, fused_scan_sub_, cmd_vel_sub_;
		ros::Time initial_trigger_time_;
		double trigger_duration_;

		bool fused_scan_init_, simulate_path_init_, initial_trigger_init_;
		shared_ptr<sensor_msgs::PointCloud> fused_scan_;
		shared_ptr<nav_msgs::Path> simulate_path_;
		double trigger_min_dist_, trigger_max_dist_, trigger_dist_;
		double trigger_bound_width_, trigger_bound_dist_, trigger_bound_time_;
		double early_trigger_ratio_;
		double trigger_max_vel_;

		void simulatePathCallBack(const nav_msgs::PathPtr pathIn);
		void fusedScanCallBack(const sensor_msgs::PointCloudPtr scanIn);
		void cmdVelCallBack(const geometry_msgs::TwistConstPtr velIn);
		void triggerReasonTimer(const ros::TimerEvent& e);
	};

}  // Perception


#endif /* OBSTREACT_REPLAN_TRIGGER_HPP_ */
