/*
 * Simulate_Speed_Advisor.cpp
 *
 *  Created on: 7 Aug, 2014
 *      Author: liuwlz
 */


#include <Speed_Control/Simulate_Speed_Advisor.hpp>

namespace Control {
	SimulateSpeedAdvisor::
	SimulateSpeedAdvisor():
	pri_nh_("~"),
	advised_speed_(0.0),
	obst_dist_(DBL_MAX),
	fused_scan_got_(false){
		pri_nh_.param("tube_width",tube_width_,1.25);
		pri_nh_.param("max_speed",max_speed_,2.0);
		pri_nh_.param("dead_zone_size",dead_zone_range_,4.0);
		pri_nh_.param("max_speed_range",max_speed_range_,8.0);
		pri_nh_.param("max_dec",max_dec_,0.3);
		pri_nh_.param("base_frame",base_frame_,string("base_link"));
		pri_nh_.param("global_frame",global_frame_,string("map"));
		pri_nh_.param("dynamic_speed_type",dynamic_speed_type_,1);

		fused_scan_sub_ = nh_.subscribe("fused_scan_pts",1,&SimulateSpeedAdvisor::fusedScanCallBack,this);
		simulate_path_sub_ = nh_.subscribe("simulate_path",1,&SimulateSpeedAdvisor::simulatePathCallBack,this);
		risk_tube_pub_ = nh_.advertise<visualization_msgs::MarkerArray>("risk_tube",1);
	}

	SimulateSpeedAdvisor::
	~SimulateSpeedAdvisor(){

	}

	void
	SimulateSpeedAdvisor::
	fusedScanCallBack(const sensor_msgs::PointCloudPtr scanIn){
		scanIn->header.stamp = ros::Time();
		fused_scan_.header.stamp = ros::Time();
		try{
			tf_.waitForTransform(global_frame_,base_frame_,ros::Time(0),ros::Duration(0.01));
			tf_.transformPointCloud(global_frame_,*scanIn, fused_scan_);
		}
		catch(tf::LookupException& ex) {
			ROS_ERROR("No Transform available Error: %s\n", ex.what());
			return;
		}
		catch(tf::ConnectivityException& ex) {
			ROS_ERROR("Connectivity Error: %s\n", ex.what());
			return;
		}
		catch(tf::ExtrapolationException& ex) {
			ROS_ERROR("Extrapolation Error: %s\n", ex.what());
			return;
		}
		fused_scan_.header.stamp = ros::Time::now();
		fused_scan_got_ = true;
	}

	void
	SimulateSpeedAdvisor::
	simulatePathCallBack(const nav_msgs::PathPtr pathIn){
		ROS_INFO_ONCE("Got Simulated Path");
		simulate_path_ = *pathIn;
		if (fused_scan_got_)
			calculateAdvisedSpeed();
	}

	void
	SimulateSpeedAdvisor::
	calculateAdvisedSpeed(){
		if (Debug)
			ROS_INFO_ONCE("Calculate Advised Speed");
		double min_lateral_dist = DBL_MAX;
		double min_longitude_dist = DBL_MAX;
		int risk_pose_index = simulate_path_.poses.size();
		double longitude_dist = 0.0;
		for(vector<geometry_msgs::PoseStamped>::iterator iter_path = simulate_path_.poses.begin();
				iter_path != simulate_path_.poses.end()-1;
				iter_path ++){
			longitude_dist += distBetweenPosewithPose(*iter_path, *(iter_path+1));
			for (vector<geometry_msgs::Point32>::iterator iter_scan = fused_scan_.points.begin();
					iter_scan != fused_scan_.points.end()-1;
					iter_scan ++){
				double dist_tmp = distBetweenPosewithPoint(*iter_path,*iter_scan);
				if (dist_tmp > tube_width_)
					continue;
				min_lateral_dist = min_lateral_dist < dist_tmp ? min_lateral_dist : dist_tmp;
				min_longitude_dist = min_longitude_dist < longitude_dist ? min_longitude_dist : longitude_dist;
				risk_pose_index = min_longitude_dist < longitude_dist ? risk_pose_index : (iter_path-simulate_path_.poses.begin());
			}
		}
		if (dynamic_speed_type_ == 1){
			double risk_dist = (min_longitude_dist - dead_zone_range_) >= 0.0 ? (min_longitude_dist - dead_zone_range_) : 0.0;
			advised_speed_ = sqrt(2.0*max_dec_*risk_dist);
		}
		else{
			double ratio = log(max_speed_ + 1.0)/max_speed_range_;
			advised_speed_ = exp((min_longitude_dist - dead_zone_range_)*ratio) - 1.0;
		}
		boundSpeed(advised_speed_);
		obst_dist_ = min_longitude_dist;
		visualizeRiskTube(risk_pose_index);
	}

	void
	SimulateSpeedAdvisor::
	boundSpeed(double& speedIn){
		speedIn = speedIn < 0 ? 0 : speedIn;
		speedIn = speedIn > max_speed_ ? max_speed_ : speedIn;
	}

	void
	SimulateSpeedAdvisor::
	visualizeRiskTube(int riskPoseIndexIn){
		//ROS_INFO("Simulate Path size: %d, Dangerous pose Index: %d",simulate_path_.poses.size(),riskPoseIndexIn);
		visualization_msgs::MarkerPtr safe_marker= shared_ptr<visualization_msgs::Marker>(new visualization_msgs::Marker());
		visualization_msgs::MarkerPtr danger_marker= shared_ptr<visualization_msgs::Marker>(new visualization_msgs::Marker());
		visualization_msgs::MarkerArrayPtr risk_tube = shared_ptr<visualization_msgs::MarkerArray>(new visualization_msgs::MarkerArray());
		makeMarker(safe_marker,1);
		for(vector<geometry_msgs::PoseStamped>::iterator iter = simulate_path_.poses.begin();
				iter!= simulate_path_.poses.begin() + riskPoseIndexIn;
				iter ++){
			geometry_msgs::Point point_tmp;
			point_tmp.x = iter->pose.position.x;
			point_tmp.y = iter->pose.position.y;
			safe_marker->points.push_back(point_tmp);
		}
		makeMarker(danger_marker,2);
		for(vector<geometry_msgs::PoseStamped>::iterator iter = simulate_path_.poses.begin() + riskPoseIndexIn;
				iter != simulate_path_.poses.end();
				iter ++){
			geometry_msgs::Point point_tmp;
			point_tmp.x = iter->pose.position.x;
			point_tmp.y = iter->pose.position.y;
			danger_marker->points.push_back(point_tmp);
		}
		risk_tube->markers.push_back(*safe_marker);
		risk_tube->markers.push_back(*danger_marker);
		risk_tube_pub_.publish(*risk_tube);
	}

	void
	SimulateSpeedAdvisor::
	makeMarker(visualization_msgs::MarkerPtr& markerIn, int markerType){
		markerIn->header.frame_id = global_frame_;
		markerIn->header.stamp = ros::Time::now();
		markerIn->action = visualization_msgs::Marker::ADD;
		markerIn->lifetime = ros::Duration(0.1);
		markerIn->type = visualization_msgs::Marker::SPHERE_LIST;
		markerIn->scale.x = tube_width_;
		markerIn->scale.y = tube_width_;
		markerIn->scale.z = 0.02;

		if(markerType == 1){
			markerIn->ns = "Safe";
			markerIn->color.r = 0.0;
			markerIn->color.g = 1.0;
			markerIn->color.b = 0.0;
			markerIn->color.a = 0.2;
		}
		if(markerType == 2){
			markerIn->ns = "Danger";
			markerIn->color.r = 1.0;
			markerIn->color.g = 0.0;
			markerIn->color.b = 0.0;
			markerIn->color.a = 0.2;
		}
	}

	double
	SimulateSpeedAdvisor::
	distBetweenPosewithPoint(const geometry_msgs::PoseStamped& poseIn, const geometry_msgs::Point32& pointIn){
		double x_diff = pointIn.x - poseIn.pose.position.x;
		double y_diff = pointIn.y - poseIn.pose.position.y;
		return sqrt(x_diff*x_diff + y_diff*y_diff);
	}

	double
	SimulateSpeedAdvisor::
	distBetweenPosewithPose(const geometry_msgs::PoseStamped& poseInA, const geometry_msgs::PoseStamped& poseInB){
		double x_diff = poseInA.pose.position.x - poseInB.pose.position.x;
		double y_diff = poseInA.pose.position.y - poseInB.pose.position.y;
		return sqrt(x_diff*x_diff + y_diff*y_diff);
	}

}  // namespace Control

