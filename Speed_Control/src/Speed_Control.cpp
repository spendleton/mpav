/*
 * Speed_Control.cpp
 *
 *  Created on: 2 Aug, 2014
 *      Author: liuwlz
 */

#include <Speed_Control/Speed_Control.hpp>

namespace Control {

	template<class AdvisedSpeedType>
	SpeedControl<AdvisedSpeedType>::
	SpeedControl():
	advised_speed_reasoner_(),
	filter_time_(ros::Time::now()),
	speed_state_ (Normal){

		advised_speed_reasoner_ = shared_ptr<AdvisedSpeedType>(new AdvisedSpeedType());

		pri_nh_.param("base_frame", base_frame_, string("base_link"));
		pri_nh_.param("global_frame", global_frame_, string("map"));
		pri_nh_.param("replan_vel", replan_vel_, 0.8);
		pri_nh_.param("slow_move_ratio", slow_move_ratio_, 0.6);

		cmd_vel_pub_ = nh_.advertise<geometry_msgs::TwistStamped>("cmd_sm",1);
		speed_control_timer_ = nh_.createTimer(ros::Duration(0.05), &SpeedControl::SpeedControlTimer, this);
	}

	template<class AdvisedSpeedType>
	SpeedControl<AdvisedSpeedType>::
	~SpeedControl(){

	}

	template<class AdvisedSpeedType>
	void
	SpeedControl<AdvisedSpeedType>::
	NormalPlanSpeedControl(){
		ROS_INFO_ONCE("Normal Speed");
    	cmd_vel_.twist.linear.x = advised_speed_reasoner_->getAdvisedSpeed();
	}

	template<class AdvisedSpeedType>
	void
	SpeedControl<AdvisedSpeedType>::
	SlowMoveSpeedControl(){
		ROS_INFO_ONCE("SlowMove Speed");
		cmd_vel_.twist.linear.x = slow_move_ratio_* advised_speed_reasoner_->getAdvisedSpeed();
	}

	template<class AdvisedSpeedType>
	void
	SpeedControl<AdvisedSpeedType>::
	ReplanSpeedControl(){
		ROS_INFO_ONCE("Replan Speed");
		cmd_vel_.twist.linear.x = replan_vel_;
	}

	template<class AdvisedSpeedType>
	void
	SpeedControl<AdvisedSpeedType>::
	TempStopControl(){
		ROS_INFO("TempStop Speed");
		cmd_vel_.twist.linear.x = 0;
	}

	template<class AdvisedSpeedType>
	void
	SpeedControl<AdvisedSpeedType>::
	SetStatus(int status){
		ROS_INFO("Reset Speed Status with id: %d", status);
		speed_state_ = SPEEDSTATUS(status);
	}

	template<class AdvisedSpeedType>
	void
	SpeedControl<AdvisedSpeedType>::
	SpeedControlTimer(const ros::TimerEvent &e){
		ROS_INFO_ONCE("Speed Control Spining");
		switch(speed_state_){
			case Normal:
				NormalPlanSpeedControl();
				break;
			case SlowMove:
				SlowMoveSpeedControl();
				break;
			case Replan:
				ReplanSpeedControl();
				break;
			case TempStop:
				TempStopControl();
				break;
			default:
				NormalPlanSpeedControl();
		}
		cmd_vel_.header.stamp = ros::Time::now();
    	cmd_vel_pub_.publish(cmd_vel_);
	}

	template class SpeedControl<SimulateSpeedAdvisor>;

}
