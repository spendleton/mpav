/*
 * GoalGenerator.hpp
 *
 *  Created on: Aug 11, 2014
 *      Author: liuwlz
 */

#ifndef GOAL_GENERATOR_HPP_
#define GOAL_GENERATOR_HPP_

#include <stdlib.h>

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <MPAVUtil/Range.hpp>

using namespace std;
using namespace boost;

namespace MotionPlan{

	typedef geometry_msgs::PoseStamped Pose;

	class GoalGenerator{

	public:

		GoalGenerator();
		virtual ~GoalGenerator();

		ros::NodeHandle nh_, pri_nh_;

		tf::TransformListener tf_;

		Pose goal_, vehicle_pose_;

		string global_frame_, base_frame_;
		nav_msgs::PathPtr reference_path_;
		int waypoint_no_;
		double vehicle_path_dist_, waypoint_increment_;

		void initReferencePath(const nav_msgs::Path global_path);
		void denseReferencePath(nav_msgs::Path pathIn);
		virtual bool transformGoalPose(Pose poseIn, Pose &poseOut);
		virtual bool getVehiclePose();
		virtual void getNearestWaypoint();
		virtual void getNearestWaypoint(geometry_msgs::PoseStamped PoseIn);

		virtual int getWaypointNo(){return waypoint_no_;}
		double distBetweenPose(const geometry_msgs::PoseStamped& poseA, const geometry_msgs::PoseStamped& poseB);
		double boundAnglePNPI(double angleIn);
		double getYawOfPose(const geometry_msgs::PoseStamped& poseIn);
		inline double Distance(double x_1, double y_1, double x_2, double y_2){
			return sqrt((x_2-x_1)*(x_2-x_1) + (y_2-y_1)*(y_2-y_1) );
		}
	};
}

#endif /* GOAL_GENERATOR_HPP_ */
