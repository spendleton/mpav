/*
 * ReplanGoal.hpp
 *
 *  Created on: Nov 19, 2013
 *      Author: liuwlz
 */

#ifndef OBST_AVOID_GOAL_HPP_
#define OBST_AVOID_GOAL_HPP_

#include <ros/ros.h>
#include <ros/console.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>
#include <Goal_Generator/Goal_Generator.hpp>

namespace MotionPlan{

	class ObstAvoidGoal:public GoalGenerator{

	public:
		ObstAvoidGoal();
		virtual ~ObstAvoidGoal();
		void makeSubGoal();

		ros::Publisher obst_avoid_goal_pub_;
		geometry_msgs::PoseStamped sub_goal_;
		nav_msgs::Path remain_path_;
		double replan_horizon_;

		void setReplanHorizon(double horizonIn){replan_horizon_ = horizonIn;};
	};
}

#endif /* OBST_AVOID_GOAL_HPP_ */
