/*
 * UTurn_Goal.cpp
 *
 *  Created on: 25 Aug, 2014
 *      Author: liuwlz
 */


#include <Goal_Generator/UTurn_Goal.hpp>

namespace MotionPlan{
	UTurnGoal::
	UTurnGoal():
	goal_type_(-1){
		uturn_goal_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("uturn_goal",1);
	}

	UTurnGoal::
	~UTurnGoal(){

	}

	void
	UTurnGoal::
	getNearestWaypoint(geometry_msgs::PoseStamped PoseIn){
		ROS_INFO("Check Nearest Waypoint for Pose: %f, %f", PoseIn.pose.position.x, PoseIn.pose.position.y);
		double min_dist = DBL_MAX;
		int min_index = -1;
		for( vector<geometry_msgs::PoseStamped>::iterator iter = reference_path_->poses.begin();
				iter != reference_path_->poses.end()-1;
				iter++ ){
			double linear_dist = Distance(PoseIn.pose.position.x, PoseIn.pose.position.y,
					iter->pose.position.x, iter->pose.position.y);
			double yaw = atan2((iter+1)->pose.position.y-iter->pose.position.y, (iter+1)->pose.position.x - iter->pose.position.x);
			double yaw_dist = fabs(boundAnglePNPI(yaw - boundAnglePNPI(getYawOfPose(PoseIn))));
			double dist = linear_dist + yaw_dist;
			if(dist < min_dist){
				min_index = iter - reference_path_->poses.begin();
				min_dist = dist;
			}
		}
		waypoint_no_ = min_index;
		vehicle_path_dist_ = min_dist;
	}

	void
	UTurnGoal::
	makeUTurnGoal(){

		getVehiclePose();
		geometry_msgs::PoseStamped counter_vehicle_pose;
		counter_vehicle_pose.pose.position = vehicle_pose_.pose.position;
		counter_vehicle_pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0.0,0.0, getYawOfPose(vehicle_pose_)-M_PI);

		if (goal_type_ == -1){
			while (!getVehiclePose()){
				ROS_INFO("Goal Generator trying to get vehicle pose!!1");
			}
			uturn_goal_= counter_vehicle_pose;
		}
		else{
			remain_path_.poses.clear();
			getNearestWaypoint(counter_vehicle_pose);
			vector<geometry_msgs::PoseStamped>::iterator iter = reference_path_->poses.begin() + waypoint_no_;
			double yaw = atan2((iter+1)->pose.position.y-iter->pose.position.y, (iter+1)->pose.position.x - iter->pose.position.x);
			uturn_goal_.pose.position = iter->pose.position;
			uturn_goal_.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0.0,0.0,yaw);
			remain_path_.poses.insert(remain_path_.poses.end(), iter, reference_path_->poses.end());
		}
		uturn_goal_.header.frame_id= global_frame_;
		uturn_goal_.header.stamp=ros::Time::now();
		uturn_goal_pub_.publish(uturn_goal_);
	}
}
