/*
 * ObstAvoidGoal.cpp
 *
 *  Created on: Nov 29, 2013
 *      Author: liuwlz
 */

#include <Goal_Generator/Obst_Avoid_Goal.hpp>

namespace MotionPlan{
	ObstAvoidGoal::
	ObstAvoidGoal():
	GoalGenerator(){
		pri_nh_.param("replan_horizon",replan_horizon_,10.0);
		obst_avoid_goal_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("obst_avoid_goal",1);
	}

	ObstAvoidGoal::
	~ObstAvoidGoal(){

	}

	void
	ObstAvoidGoal::
	makeSubGoal(){
		getNearestWaypoint();
		remain_path_.poses.clear();
		double dist_increment = vehicle_path_dist_;
		for (vector<geometry_msgs::PoseStamped>::iterator iter = reference_path_->poses.begin() + waypoint_no_;
				iter != reference_path_->poses.end()-1;
				iter ++){
			double yaw = atan2((iter+1)->pose.position.y-iter->pose.position.y, (iter+1)->pose.position.x - iter->pose.position.x);
			double yaw_diff = boundAnglePNPI(boundAnglePNPI(getYawOfPose(vehicle_pose_))-yaw);
			if (fabs(yaw_diff) > M_PI/1.5){
				cout <<"Invalid goal due to angle difference, current dist: "<<dist_increment<<endl;
				continue;
			}
			dist_increment += Distance(iter->pose.position.x, iter->pose.position.y, (iter+1)->pose.position.x, (iter+1)->pose.position.y);
			if (dist_increment > replan_horizon_ || iter == reference_path_->poses.end()-2){
				sub_goal_.pose.position.x = iter->pose.position.x;
				sub_goal_.pose.position.y = iter->pose.position.y;
				sub_goal_.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0.0,0.0,yaw);
				remain_path_.poses.insert(remain_path_.poses.end(), iter, reference_path_->poses.end());
				break;
			}
		}
		sub_goal_.header.frame_id= global_frame_;
		sub_goal_.header.stamp=ros::Time::now();
		obst_avoid_goal_pub_.publish(sub_goal_);
	}
}

