#include "Path.h"
#include <ros/console.h>

using namespace Gende;

Gende::Path::Path(Ogre::SceneManager* manager, Ogre::SceneNode* parent_node,
                  std::string &name,
                  const Ogre::Vector3& position,
                  std::string meshName)
    : Object( manager ),
      manager_(manager),
      path_name( name ),
      meshName_(meshName)
{
    if (!parent_node)
    {
      parent_node = manager->getRootSceneNode();
    }
    manual_object_ =  manager->createManualObject(name);
    scene_node_ = parent_node->createChildSceneNode();
    scene_node_->attachObject(manual_object_);
    setPosition(position);
    //
//    Ogre::ManualObject* manual_object_2 =  manager->createManualObject(name+"adf");
    appendVertex(position);
}
void Path::setPosition( const Ogre::Vector3& position )
{
  scene_node_->setPosition( position );
}
void Path::setOrientation( const Ogre::Quaternion& orientation )
{
  scene_node_->setOrientation( orientation );
}
void Path::setScale( const Ogre::Vector3& scale )
{
  scene_node_->setScale( scale );
}
void Path::setColor( float r, float g, float b, float a )
{
  manual_object_material_->getTechnique(0)->getPass(0)->setDiffuse(r,g,b,a);
}

const Ogre::Vector3& Path::getPosition()
{
  return scene_node_->getPosition();
}

const Ogre::Quaternion& Path::getOrientation()
{
  return scene_node_->getOrientation();
}

void Path::setUserData( const Ogre::Any& data )
{
  manual_object_->setUserAny( data );
}
void Path::emit_create_info()
{
    ROS_INFO("send path id");
    QString pathid = QString().fromStdString(path_name);
    Q_EMIT emit_path_info(pathid);
    //
    PathVertex* pv = vertexPool.at(0);
    Q_EMIT emit_vertex_info(pv);
}

void Path::appendVertex(const Ogre::Vector3 &position)
{
    std::string vertex_name = generateVertexName();
    PathVertex* pv = new PathVertex(manager_,manager_->getRootSceneNode(),vertex_name,position,meshName_);
    pv->setParentPath(this);
    vertexPool.push_back(pv);
    // create pathline
    if(vertexPool.size()>1)
    {
        PathVertex* start_v = vertexPool[vertexPool.size()-2];
        std::string line_name = start_v->getName()+"__"+pv->getName();
        PathLine* line_ = new PathLine(manager_,manager_->getRootSceneNode(),line_name,start_v,pv);
        linePool.push_back(line_);
        // connect vertex and edge
        start_v->setUpLine(line_);
        pv->setDownLine(line_);
    }
    //
    Q_EMIT emit_vertex_info(pv);
}
std::string Path::generateVertexName()
{
    char name[128];
    sprintf(name, "%s_vertex_%d", path_name.c_str(),vertexPool.size()+1);
    std::string vertex_name( name );
    return vertex_name;
}
int Path::getVertexIndex(PathVertex *pv)
{
    int res=-1;
    for (int i=0;i<vertexPool.size();++i)
    {
        if(pv->getName() == vertexPool[i]->getName())
        {
            return i+1;
        }
    }
    return res;
}
