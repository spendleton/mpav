#include "pathvertex.h"

using namespace Gende;

PathVertex::PathVertex(Ogre::SceneManager* manager,
                       Ogre::SceneNode* parent_node,
                       const std::string& name,
                       const Ogre::Vector3& position,
                       const std::string& meshName)
    :manager_(manager),
      meshName_(meshName),
      scene_node_(parent_node),
      name_(name),
      upLine_(NULL),
      downLine_(NULL),
      parentPath(NULL)
{
    if (!parent_node)
    {
      parent_node = manager->getRootSceneNode();
    }

    vertex_ = manager->createEntity(name, meshName );
    scene_node_ = parent_node->createChildSceneNode();
    scene_node_->attachObject( vertex_ );
    scene_node_->setVisible( true );
    scene_node_->setPosition( position );

//    vertex_ = manager->createManualObject(name);

//    float const radius = 1;

//    // accuracy is the count of points (and lines).
//    // Higher values make the circle smoother, but may slowdown the performance.
//    // The performance also is related to the count of circles.
//    float const accuracy = 35;

//    vertex_->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_STRIP);

//    unsigned point_index = 0;
//    for(float theta = 0; theta <= 2 * Ogre::Math::PI; theta += Ogre::Math::PI / accuracy) {
//       vertex_->position(radius * cos(theta), 0, radius * sin(theta));
//       vertex_->index(point_index++);
//    }
//    vertex_->index(0); // Rejoins the last point to the first.

//    vertex_->end();

//    parent_node->createChildSceneNode()->attachObject(vertex_);
}
void PathVertex::setPosition( const Ogre::Vector3& pos)
{
    scene_node_->setPosition(pos);
    updateLines();
}
void PathVertex::updateLines()
{
    if(upLine_)
    {
        upLine_->updatePosition();
    }
    if(downLine_)
    {
        downLine_->updatePosition();
    }
}
