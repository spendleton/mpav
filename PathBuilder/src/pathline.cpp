#include "pathline.h"

using namespace Gende;

PathLine::PathLine(Ogre::SceneManager* manager,
                   Ogre::SceneNode* parent_node,
                   const std::string& name,
                   PathVertex *startVertex,
                   PathVertex *endVertex)
    : manager_(manager),
      name_(name),
      startVertex_(startVertex),
      endVertex_(endVertex),
      parentPath(NULL)
{
    if (!parent_node)
    {
      parent_node = manager->getRootSceneNode();
    }

    line_ = manager->createManualObject(name);

    line_->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_STRIP);

    line_->position(startVertex->getPosition());
    line_->position(endVertex->getPosition());

    line_->end();

    scene_node_ = parent_node->createChildSceneNode();
    scene_node_->attachObject(line_);
}
void PathLine::updatePosition()
{
    line_->beginUpdate(0);

    line_->position(startVertex_->getPosition());
    line_->position(endVertex_->getPosition());

    line_->end();
}
