#ifndef PATHVERTEX_H
#define PATHVERTEX_H

#include <OGRE/OgreManualObject.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreMaterial.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreEntity.h>

#include "Path.h"
#include "pathline.h"

#include <string>

namespace Gende
{
class Path;
class PathLine;

class PathVertex
{
public:
    PathVertex(Ogre::SceneManager* manager,
               Ogre::SceneNode* parent_node,
               const std::string& name,
               const Ogre::Vector3& position,
               const std::string& meshName);
    const Ogre::Vector3 getPosition() { return scene_node_->getPosition(); }
    void setPosition(const Ogre::Vector3& pos);
    void updateLines();
    std::string getName() { return name_; }
    void setUpLine(PathLine* line) { upLine_ = line; }
    void setDownLine(PathLine* line) { downLine_ = line; }
    void setParentPath(Path* p) { parentPath = p; }
    Ogre::Entity* getEntity() { return vertex_; }

    Path* getParentPath() { return parentPath; }
private:
    std::string name_;
    Ogre::Entity* vertex_;
    Ogre::SceneManager* manager_;
    std::string meshName_;
    Ogre::SceneNode* scene_node_;
    Path *parentPath;

    //o----->o----->
    //  down    up
    PathLine* upLine_;
    PathLine* downLine_;
};

}// end namespace Gende
#endif // PATHVERTEX_H
