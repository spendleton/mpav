#ifndef PATHLINE_H
#define PATHLINE_H

#include <OGRE/OgreManualObject.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreMaterial.h>
#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreEntity.h>

#include <vector>

#include "Path.h"
#include "pathvertex.h"

namespace Gende
{
class Path;
class PathVertex;

class PathLine
{
public:
    PathLine(Ogre::SceneManager* manager,
             Ogre::SceneNode* parent_node,
             const std::string& name,
             PathVertex *startVertex,
             PathVertex *endVertex);
    void updatePosition();
    void setParentPath(Path* p) { parentPath = p; }
private:
    std::string name_;
    Ogre::ManualObject* line_;
    Ogre::SceneManager* manager_;
    Ogre::SceneNode* scene_node_;
    Path* parentPath;

    PathVertex *startVertex_;
    PathVertex *endVertex_;
};

}// end namespace Gende
#endif // PATHLINE_H
