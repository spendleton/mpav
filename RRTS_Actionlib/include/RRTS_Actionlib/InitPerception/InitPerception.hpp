/*
 * InitPerception.hpp
 *
 *  Created on: 4 Aug, 2014
 *      Author: liuwlz
 */

#ifndef INITPERCEPTION_HPP_
#define INITPERCEPTION_HPP_

#include <RRTS_Actionlib/Navigation/NaviSM.hpp>

namespace BehaviorPlan{

	struct InitPerceptionSM:sc::state<InitPerceptionSM, NaviSM>{
		typedef mpll::list<
				sc::custom_reaction<Ev_PerceptionInitilized>
				>reactions;

		typedef sc::state<InitPerceptionSM, NaviSM> base;
		InitPerceptionSM(my_context ctx):base(ctx){
			ROS_WARN("Enter InitPeception");
			context<NaviSM>().vehicle_ready_ = false;
			context<NaviSM>().metric_map_->prior_info_init_ = false;
			if (context<NaviSM>().metric_map_type_!=1){
				//context<NaviSM>().extend_metric_map_->initReferencePath(context<NaviSM>().reference_path_);
				boost::static_pointer_cast<ExtendMetricMap>(context<NaviSM>().metric_map_)->initReferencePath(context<NaviSM>().reference_path_);
			}
			context<NaviSM>().metric_map_->metric_map_timer_.start();
			context<NaviSM>().obst_avoid_goal_->initReferencePath(context<NaviSM>().reference_path_);
			context<NaviSM>().uturn_goal_->initReferencePath(context<NaviSM>().reference_path_);
			post_event(Ev_PerceptionInitilized());
		}

		virtual ~InitPerceptionSM(){

		}

		sc::result react(const Ev_PerceptionInitilized& ){
			return transit<MovingSM>();
		}
	};
}  // namespace BehaviorPlan

#endif /* INITPERCEPTION_HPP_ */
