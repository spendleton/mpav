/*
 * NaviSM.hpp
 *
 *  Created on: Nov 29, 2013
 *      Author: liuwlz
 */

#ifndef NAVISM_HPP_
#define NAVISM_HPP_

#include <vector>
#include <string>

#include <boost/statechart/state.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/event.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/mpl/list.hpp>

#include <RRTS_Actionlib/Navigation/NaviEvents.hpp>

#include <Metric_Map/Extend_Metric_Map.hpp>
#include <Metric_Map/Prior_Metric_Map.hpp>
#include <OMPLRRTS/RRTS_Dubins.hpp>
#include <OMPLRRTS/RRTS_ReedShepp.hpp>
#include <Goal_Generator/Obst_Avoid_Goal.hpp>
#include <Goal_Generator/UTurn_Goal.hpp>

namespace sc = boost::statechart;
namespace mpll = boost::mpl;
using namespace std;

namespace BehaviorPlan{
	/**
	 * Forward definition for states and state machine
	 */
	struct MissionWaitingSM;
	struct InitPerceptionSM;
	struct MovingSM;
	struct NormPlanSM;
	struct ObstAvoidSM;
	struct UTurnSM;
	struct ManualGoalSM;
	struct naviSM;

	enum StateID {IdleCheck, Mission, Perception, Moving, Normal, ObstAvoid, Parking, UTurn};
}

#endif /* NAVISM_HPP_ */
