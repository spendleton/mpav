/*
 * MovingSM.hpp
 *
 *  Created on: Nov 29, 2013
 *      Author: liuwlz
 */

#ifndef MOVINGSM_HPP_
#define MOVINGSM_HPP_

#include <boost/statechart/state.hpp>
#include <boost/statechart/event.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/mpl/list.hpp>

#include <RRTS_Actionlib/Navigation/NaviSM.hpp>

namespace BehaviorPlan{

	struct MovingSM : sc::state<MovingSM, NaviSM, NormPlanSM>{
		typedef mpll::list<
				sc::custom_reaction<Ev_ReachDest>
				> reactions;
		typedef sc::state<MovingSM, NaviSM, NormPlanSM> Base;
		MovingSM(my_context ctx) : Base(ctx){
			ROS_WARN("Enter Moving");
		}

		virtual ~MovingSM(){

		}

		sc::result react(const Ev_ReachDest &){
			return transit<MissionWaitingSM>();
		}
	};
}

#endif /* MOVINGSM_HPP_ */
