/*
 * MissionWaitingSM.hpp
 *
 *  Created on: 21 Jul, 2014
 *      Author: liuwlz
 */

#ifndef MISSIONWAITINGSM_HPP_
#define MISSIONWAITINGSM_HPP_

#include <RRTS_Actionlib/Navigation/NaviSM.hpp>

namespace BehaviorPlan {

	struct MissionWaitingSM:sc::state<MissionWaitingSM, NaviSM>{
		typedef mpll::list<
				sc::custom_reaction<Ev_MissionRecived>
		>reactions;

		typedef sc::state<MissionWaitingSM, NaviSM> base;
		MissionWaitingSM(my_context ctx):base(ctx){
			ROS_WARN("Enter MissionWaiting: Waiting for the reference path!");
			context<NaviSM>().vehicle_ready_ = false;
			context<NaviSM>().metric_map_->metric_map_timer_.stop();
			context<NaviSM>().dubins_planner_->plan_timer_.stop();
			context<NaviSM>().reedshepp_planner_->plan_timer_.stop();
		}
		virtual ~MissionWaitingSM(){

		}
		sc::result react(const Ev_MissionRecived& ){
			return transit<InitPerceptionSM>();
		}

	};

}  // namespace MissionWaitingSM

#endif /* MISSIONWAITINGSM_HPP_ */
