/*
 * RRTS_Actionlib.hpp
 *
 *  Created on: 23 Aug, 2014
 *      Author: liuwlz
 */

#ifndef RRTS_ACTIONLIB_HPP_
#define RRTS_ACTIONLIB_HPP_

#include <ros/ros.h>
#include <std_msgs/Bool.h>

#include <actionlib/server/simple_action_server.h>
#include <RRTS_Actionlib/RRTSAction.h>

#include <string.h>
#include <boost/shared_ptr.hpp>
#include <boost/bind/bind.hpp>

#include <RRTS_Actionlib/Navigation/NavigationSM.hpp>
#include <RRTS_Actionlib/MissionWaiting/MissionWaitingSM.hpp>
#include <RRTS_Actionlib/InitPerception/InitPerception.hpp>
#include <RRTS_Actionlib/Moving/MovingSM.hpp>
#include <RRTS_Actionlib/NormPlan/NormPlan.hpp>
#include <RRTS_Actionlib/ObstAvoid/ObstAvoidSM.hpp>
#include <RRTS_Actionlib/UTurn/UTurnSM.hpp>
#include <RRTS_Actionlib/ManualGoal/ManualGoal.hpp>

#include <pnc_msgs/move_status.h>

using namespace std;
using namespace boost;

namespace BehaviorPlan{

	class RRTSActionlib {

	protected:
		ros::NodeHandle nh_, pri_nh_;
		ros::Subscriber reference_path_sub_, move_status_sub_, manual_goal_sub_;
		ros::Publisher rrts_path_view_pub_;
		ros::Timer status_check_timer_;

		actionlib::SimpleActionServer<RRTS_Actionlib::RRTSAction> action_server_;
		RRTS_Actionlib::RRTSFeedback feed_back_;
		RRTS_Actionlib::RRTSResult result_;

		bool init_replan_timer_;
		ros::Time replan_time;

		boost::shared_ptr<NaviSM> navi_sm_;

		bool obst_replan_, enable_replan_timer_;
		double replan_duration_tolerance_;
		double destination_reach_dist_;
		double goal_dist_;

		nav_msgs::Path combined_path_;

		string global_frame_, base_frame_, action_name_;
		typedef enum{MissionWaiting = 0, Planning, PathFound, TimeOutFailure} FEEDBACK;

	public:

		RRTSActionlib(string nameIn);
		virtual ~RRTSActionlib();

		void replanCallBack();
		void manualGoalCallBack(const geometry_msgs::PoseStamped& mannualGoalIn);
		void moveStatusCallBack(const pnc_msgs::move_status& moveStatusIn);
		void referencePathCallBack(const nav_msgs::Path& pathIn);
		void statusCheckTimer(const ros::TimerEvent& e);
		void makePlan();

	};

}  // namespace BehaviorPlan



#endif /* RRTS_ACTIONLIB_HPP_ */
