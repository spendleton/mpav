/*
 * RRTS_Actionlib.cpp
 *
 *  Created on: 23 Aug, 2014
 *      Author: liuwlz
 */
#include <RRTS_Actionlib/RRTS_Actionlib.hpp>

namespace BehaviorPlan {

	RRTSActionlib::
	RRTSActionlib(string nameIn):
	pri_nh_("~"),
	action_server_(nh_, nameIn, false),
	init_replan_timer_(false),
	action_name_(nameIn){

		/**Start of action server*/
		action_server_.registerGoalCallback(boost::bind(&RRTSActionlib::replanCallBack, this));
		action_server_.start();
		/**End of action server*/

		navi_sm_ = boost::shared_ptr<NaviSM>(new NaviSM());

		pri_nh_.param("obst_replan",obst_replan_,true);
		pri_nh_.param("enable_replan_timer",enable_replan_timer_,true);
		pri_nh_.param("planner_type",navi_sm_->planner_type_,3);
		pri_nh_.param("metric_map_type",navi_sm_->metric_map_type_,1);
		pri_nh_.param("global_frame",global_frame_, string("map"));
		pri_nh_.param("base_frame",base_frame_,string("base_link"));
		pri_nh_.param("replan_duration_tolerance",replan_duration_tolerance_,5.0);
		pri_nh_.param("destination_reach_dist",destination_reach_dist_,3.0);

		reference_path_sub_= nh_.subscribe("route_plan",0,&RRTSActionlib::referencePathCallBack,this);
		move_status_sub_ = nh_.subscribe("move_status",1,&RRTSActionlib::moveStatusCallBack,this);
		manual_goal_sub_ = nh_.subscribe("move_base_simple/goal",1,&RRTSActionlib::manualGoalCallBack, this);

		rrts_path_view_pub_ = nh_.advertise<nav_msgs::Path>("rrts_path_view",1);

		navi_sm_->dubins_planner_ = boost::shared_ptr<RRTSPlanner>(new RRTSDubins());
		navi_sm_->reedshepp_planner_ = boost::shared_ptr<RRTSPlanner>(new RRTSReedShepp());
		navi_sm_->obst_avoid_goal_ = boost::shared_ptr<GoalGenerator>(new ObstAvoidGoal());
		navi_sm_->uturn_goal_ = boost::shared_ptr<GoalGenerator>(new UTurnGoal());
		navi_sm_->metric_map_ = navi_sm_->metric_map_type_ == 1 ?
				boost::shared_ptr<MetricMap>(new PriorMetricMap()) : boost::shared_ptr<MetricMap>(new ExtendMetricMap());

		navi_sm_->initiate();

		status_check_timer_ = nh_.createTimer(ros::Duration(0.05), &RRTSActionlib::statusCheckTimer,this);
	}

	RRTSActionlib::
	~RRTSActionlib(){
		action_server_.shutdown();
	}

	void
	RRTSActionlib::
	replanCallBack(){
		goal_dist_ = action_server_.acceptNewGoal()->subgoal_dist;
		ROS_INFO("Replan actionclient CallBack with goal dist: %f", goal_dist_);

		if (navi_sm_->state_downcast<const MissionWaitingSM*>() != 0){
			feed_back_.replan_status = MissionWaiting;
			action_server_.publishFeedback(feed_back_);
			action_server_.setAborted(result_,"Aborting due to the RRTS is still waiting for mission");
			return;
		}

		if (goal_dist_ > 0){
			ROS_INFO("Obstacle Avoidance with ahead dist: %f",goal_dist_);
			static_pointer_cast<ObstAvoidGoal>(navi_sm_->obst_avoid_goal_)->setReplanHorizon(goal_dist_);
			navi_sm_->process_event(Ev_ObstAvoid());
		}
		else if (goal_dist_ == 0){
			ROS_INFO("Manual goal task!");
			navi_sm_->process_event(Ev_ManualGoal());
		}
		else{
			ROS_INFO("UTurn with type: %f", goal_dist_);
			static_pointer_cast<UTurnGoal>(navi_sm_->uturn_goal_)->setGoalType((int)goal_dist_);
			navi_sm_->process_event(Ev_UTurn());
		}
	}

	void
	RRTSActionlib::
	referencePathCallBack(const nav_msgs::Path& pathIn){
		if (navi_sm_->reference_path_.poses.size() == pathIn.poses.size())
			return;
		if (navi_sm_->state_downcast<const MissionWaitingSM*>() != 0){
			ROS_INFO("Transit to Normal Plan");
			navi_sm_->reference_path_ = pathIn;
			navi_sm_->process_event(Ev_MissionRecived());
		}
	}

	void
	RRTSActionlib::
	moveStatusCallBack(const pnc_msgs::move_status& moveStatusIn){
		if(moveStatusIn.dist_to_goal < destination_reach_dist_)
			navi_sm_->process_event(Ev_ReachDest());
	}

	void
	RRTSActionlib::
	manualGoalCallBack(const geometry_msgs::PoseStamped& manualGoalIn){
		if (navi_sm_->state_downcast<const ManualGoalWaiting*>() != 0){
			ROS_INFO("Transit to mannual goal planning!");
			navi_sm_->mannual_goal_ = manualGoalIn;
			navi_sm_->process_event(Ev_ManualGoalGot());
		}
		else
			ROS_ERROR("Manual goal cannot be processed at current state!");
	}

	void
	RRTSActionlib::
	statusCheckTimer(const ros::TimerEvent& event){

 		//ROS_INFO("Action Server is active");

		if (!action_server_.isActive()){
			//ROS_INFO("Server is not active!");
			return;
		}

		double replan_duration = 0.0;
		if (navi_sm_->state_downcast<const Replan*>() != 0 || navi_sm_->state_downcast<const UTurnSM*>() != 0){
			feed_back_.replan_status = Planning;
			action_server_.publishFeedback(feed_back_);
		}
		if(enable_replan_timer_){
			if (navi_sm_->state_downcast<const Replan*>() != 0 || navi_sm_->state_downcast<const UTurnSM*>() != 0){
				if (!init_replan_timer_){
					replan_time = ros::Time::now();
					init_replan_timer_ = true;
				}
				else{
					replan_duration = (ros::Time::now() - replan_time).toSec();
				}
				if (replan_duration > replan_duration_tolerance_){

					ROS_WARN("Replan Timeout with duration %f, try to reverse to Normal Plan", replan_duration);
					init_replan_timer_ = false;
					if (navi_sm_->state_downcast<const Replan*>() != 0)
						navi_sm_->process_event(Ev_ReplanPathFound());
					if (navi_sm_->state_downcast<const UTurnSM*>() != 0)
						navi_sm_->process_event(Ev_UTurnPathFound());

					feed_back_.replan_status = TimeOutFailure;
					action_server_.publishFeedback(feed_back_);
					action_server_.setAborted(result_,"Planning Timeout");
				}
			}
		}

		if (navi_sm_->dubins_planner_->plan_status_ == 1){

			init_replan_timer_ = false;
			navi_sm_->process_event(Ev_ReplanPathFound());

			makePlan();
			result_.replan_path = combined_path_;
			
			feed_back_.replan_status = PathFound;
			result_.replan_path.header.stamp = ros::Time::now();
			result_.replan_path.header.frame_id = global_frame_;

			rrts_path_view_pub_.publish(result_.replan_path);

			action_server_.publishFeedback(feed_back_);
			action_server_.setSucceeded(result_, "Successfully Found a Solution");
		}

		if (navi_sm_->reedshepp_planner_->plan_status_ == 1){
			ROS_INFO("Reedshepp path got!");
			init_replan_timer_ = false;
			navi_sm_->process_event(Ev_UTurnPathFound());
			if (goal_dist_ == -1.0){
				result_.replan_path = navi_sm_->reedshepp_planner_->getSolutionPath();
			}
			if (goal_dist_ != -1.0){
				makePlan();
				result_.replan_path = combined_path_;
			}

			//If the path is found, set succeed
			feed_back_.replan_status = PathFound;
			result_.replan_path.header.stamp = ros::Time::now();
			result_.replan_path.header.frame_id = global_frame_;

			rrts_path_view_pub_.publish(result_.replan_path);

			action_server_.publishFeedback(feed_back_);
			action_server_.setSucceeded(result_, "Successfully Found a Solution");
		}
	}

	void
	RRTSActionlib::
	makePlan(){
		if (goal_dist_ > 0){
			combined_path_ = navi_sm_->dubins_planner_->getSolutionPath();
			combined_path_.poses.insert(combined_path_.poses.end(),
					static_pointer_cast<ObstAvoidGoal>(navi_sm_->obst_avoid_goal_)->remain_path_.poses.begin(),
					static_pointer_cast<ObstAvoidGoal>(navi_sm_->obst_avoid_goal_)->remain_path_.poses.end());
		}
		else{
			combined_path_ = navi_sm_->reedshepp_planner_->getSolutionPath();
			combined_path_.poses.insert(combined_path_.poses.end(),
					static_pointer_cast<UTurnGoal>(navi_sm_->uturn_goal_)->remain_path_.poses.begin(),
					static_pointer_cast<UTurnGoal>(navi_sm_->uturn_goal_)->remain_path_.poses.end());
		}
		combined_path_.header.frame_id = global_frame_;
		combined_path_.header.stamp = ros::Time::now();
	}

}  // namespace BehaviorPlan
