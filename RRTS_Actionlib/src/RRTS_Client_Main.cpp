/*
 * RRTS_Client.cpp
 *
 *  Created on: 23 Aug, 2014
 *      Author: liuwlz
 */



#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <RRTS_Actionlib/RRTSAction.h>

int main (int argc, char **argv){
	ros::init(argc, argv, "replan_client");

	double goal_dist_;
	ros::NodeHandle pri_nh_("~");

	pri_nh_.param("goal_dist",goal_dist_,-1.0);

	actionlib::SimpleActionClient<RRTS_Actionlib::RRTSAction> ac("replan", true);

	ROS_INFO("Waiting for action server to start.");
	if (ac.waitForServer(ros::Duration(1.0))){
		ROS_INFO("Server is connected!");
	}else{
		ROS_WARN("Failed to connect to server, aborted!");
		return 0;
	}

	ROS_INFO("Action server started, sending goal.");
	RRTS_Actionlib::RRTSGoal goal;
	goal.subgoal_dist = goal_dist_;
	ac.sendGoal(goal);

	bool finished_before_timeout = ac.waitForResult(ros::Duration(1.0));

	if (finished_before_timeout){
		actionlib::SimpleClientGoalState state = ac.getState();
		ROS_INFO("Action finished: %s",state.toString().c_str());
	}
	else{
		ac.cancelGoal();
		ROS_INFO("Action did not finish before the time out.");
	}
	ros::spin();

	return 0;
}
