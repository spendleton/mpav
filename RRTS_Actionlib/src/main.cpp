/*
 * main.cpp
 *
 *  Created on: Nov 19, 2013
 *      Author: liuwlz
 */

#include <RRTS_Actionlib/RRTS_Actionlib.hpp>

int main(int argc, char**argv){
	ros::init(argc, argv, "rrts_replan");
	BehaviorPlan::RRTSActionlib	replan(string("replan"));
	ros::spin();
	return 1;
}
