/*
 * StationInteractive.hpp
 *
 *  Created on: 23 Jul, 2014
 *      Author: liuwlz
 */

#ifndef STATIONINTERACTIVE_HPP_
#define STATIONINTERACTIVE_HPP_


#include <ros/ros.h>
#include <visualization_msgs/InteractiveMarker.h>
#include <visualization_msgs/InteractiveMarkerControl.h>
#include <visualization_msgs/InteractiveMarkerFeedback.h>
#include <interactive_markers/interactive_marker_server.h>


#include <boost/bind.hpp>

#include <RoutePlanner/StationRouting.hpp>

using namespace visualization_msgs;
using namespace interactive_markers;

namespace MissionPlan {
	class StationInteractive{
	public:
		StationInteractive();
		virtual ~StationInteractive();
		void prompt();

	private:
		ros::NodeHandle nh_, priv_nh_;
		string global_frame_, base_frame_;

		pair<int, int> station_pair_;
		int station_select_;
		int feedback_counter_;

		shared_ptr<StationRouting> station_routing_;
		shared_ptr<InteractiveMarkerServer> server_;

		void initInteractiveMarker();
		void processFeedback( const InteractiveMarkerFeedbackConstPtr &feedbackIn);
		Marker makeMarker(InteractiveMarker& intMarkerIn);
		geometry_msgs::Pose getMarkerPose(int stationID);
	};

}  // namespace MissionPlan


#endif /* STATIONINTERACTIVE_HPP_ */
