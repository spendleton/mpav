/*
 * StationSeaching.hpp
 *
 *  Created on: 3 Jul, 2014
 *      Author: liuwlz
 */

#ifndef STATIONSEACHING_HPP_
#define STATIONSEACHING_HPP_

#include <RoutePlanner/StationLoading.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

using namespace std;
using namespace boost;

namespace MissionPlan{

	class StationSearching{
	private:
		shared_ptr<StationLoading> station_loading_;

	public:

		StationSearching();
		virtual ~StationSearching();

		shared_ptr<RouteMap> route_map_;

		void searchRoute(int startIn, int endIn, vector< VertexDpter>& pathOut);
		StationPath getRoute(int startIn, int endIn);
		shared_ptr<StationLoading> getStationLoading()const{return station_loading_;};

	};
}

#endif /* STATIONSEACHING_HPP_ */
