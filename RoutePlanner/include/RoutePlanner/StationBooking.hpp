/*
 * StationBooking.hpp
 *
 *  Created on: 5 Aug, 2014
 *      Author: liuwlz
 */

#ifndef STATIONBOOKING_HPP_
#define STATIONBOOKING_HPP_

#include <ros/ros.h>
#include <visualization_msgs/InteractiveMarker.h>
#include <visualization_msgs/InteractiveMarkerControl.h>
#include <visualization_msgs/InteractiveMarkerFeedback.h>
#include <interactive_markers/interactive_marker_server.h>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/tfMessage.h>

#include <geometry_msgs/PoseStamped.h>

#include <boost/bind.hpp>
#include <RoutePlanner/StationRouting.hpp>

using namespace boost;
using namespace visualization_msgs;
using namespace interactive_markers;

namespace MissionPlan {

	typedef pair<int, int> StationCouple;
	typedef pair<StationCouple, StationCouple> Mission;
	typedef pair<StationPath, StationPath> StationPathCouple;

	class StationBooking:public StationRouting{
	public:
		StationBooking();
		virtual ~StationBooking();

		geometry_msgs::PoseStamped vehicle_pose_;

		Mission mission_;
		StationPathCouple mission_path_;
		vector<StationCouple> station_paired_;
		int vehicle_current_station_;
		bool first_station_init_, paired_station_init_;
		double dist_to_goal_;

		tf::TransformListener tf_;

		typedef enum{MissionWaiting, ConfirmPickUp, ApproachPickUp, ConfirmDest, ApproachDest, ConfirmReach}MissionStatus;
		MissionStatus mission_status_;

		void initFirstStation();
		void initPairedStation();
		void prompt();
		void askForPermission(int permissionType);
		void publishPath(int pathType);
		void checkDistToGoal(int goalType);
		void routeBooking(StationCouple start, StationCouple end);
		void reasonMissionStatus();

		bool getVehiclePose();
		double getVehiclePathPointDist(const PathPoint& pointInA, const PathPoint& pointInB);
	};

}  // namespace MissionPlan


#endif /* STATIONBOOKING_HPP_ */
