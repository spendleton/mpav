/*
 * SvgPath.cpp
 *
 *  Created on: 11 Jul, 2014
 *      Author: liuwlz
 */

#ifndef SVGPATH_CPP_
#define SVGPATH_CPP_

#include <RoutePlanner/SvgPath.hpp>

namespace MissionPlan {

	StationPath::
	StationPath():
	exist_(false),
	distance_(DBL_MAX),
	speed_limit_(0.0),
	congestion_(DBL_MAX){

	}

	StationPath&
	StationPath::
	operator=(const StationPath &stationPathIn){
		if (this == &stationPathIn)
			return *this;
		exist_ = stationPathIn.exist_;
		distance_ = stationPathIn.distance_;
		congestion_ = stationPathIn.congestion_;
		speed_limit_ = stationPathIn.speed_limit_;
		point_set_ = stationPathIn.point_set_;
		return *this;
	}

	StationPath&
	StationPath::
	operator+(const StationPath &stationPathIn){
		exist_ = exist_ && stationPathIn.exist_;
		distance_ += stationPathIn.distance_;
		congestion_ += stationPathIn.congestion_;
		speed_limit_ = speed_limit_ < stationPathIn.speed_limit_ ? speed_limit_ : stationPathIn.speed_limit_;
		point_set_.insert(point_set_.end(),stationPathIn.point_set_.begin(), stationPathIn.point_set_.end());
		return *this;
	}

	SvgPath::
	SvgPath(){

	}

	SvgPath::
	SvgPath(const char* fileNameIn, double resolution):
	resolution_(resolution){
		svg_doc_ = shared_ptr<TiXmlDocument>(new TiXmlDocument());
		loadFile(fileNameIn);
	}

	SvgPath::
	~SvgPath(){

	}

	string
	SvgPath::
	tildeExpand(const char *path){
		wordexp_t p;
		wordexp(path, &p, 0);
		char **w = p.we_wordv;
		assert(p.we_wordc==1);
		string res = w[0];
		wordfree(&p);
		return res;
	}

	void
	SvgPath::
	loadFile(const char* fileNameIn){
		cout << "Loading SvgFile with name: "<<fileNameIn<<endl;
	    TiXmlElement* svgElement;
	    if (svg_doc_->LoadFile(tildeExpand(fileNameIn).c_str())){
	        svgElement = svg_doc_->FirstChildElement();
	        const char * s = svgElement->Value();
	        if( strcmp(s,"svg")==0 ){
	        	cout<<"Svg file loaded"<<endl;
	        }
	        else
	            throw (string("Is SVG file loaded? The value found is ")+s);
	    }
	    else{
	        throw (string("Failed to load file ")+fileNameIn);
	    }
	    this->getResolution();
	}

	void
	SvgPath::
	getResolution(){
		bool res_set = false;
	    TiXmlElement* svgElement;
	    svgElement = svg_doc_->FirstChildElement();
	    TiXmlNode* svg_node_;
		for (svg_node_ = svgElement->FirstChild(); svg_node_ != 0; svg_node_ = svg_node_->NextSibling()){
			TiXmlElement* childElement= svg_node_->ToElement();
			if( strcmp(childElement->Value(),"path")==0 ){
				const char* value = childElement->Attribute("id");
				if( strncmp("RES",value,3)== 0 ){
					char res_char[strlen(value)-3];
					strcpy(res_char,&value[3]);
					resolution_ = atof(res_char);
					res_set = true;
					cout << "Loaded Resolution: "<<resolution_<<endl;
				}
			}
		}
		if (!res_set)
			cerr<<"Resolution is not set, use 0.1 as default"<<endl;
	}

	void
	SvgPath::
	getStationList(vector<StationPair> &stationListOut){
		int num_station = 0;
	    TiXmlElement* svgElement;
	    svgElement = svg_doc_->FirstChildElement();
	    TiXmlNode* svg_node_;
	    for ( svg_node_ = svgElement->FirstChild(); svg_node_ != 0; svg_node_ = svg_node_->NextSibling()){
	        TiXmlElement* childElement= svg_node_->ToElement();
	        if( strcmp(childElement->Value(),"path")==0 ){
	            const char* value = childElement->Attribute("id");
	            assert( value!=NULL );
	            if( strncmp("ST",value,2)== 0 ){
	            	char stationName[strlen(value)-2];
	            	strcpy(stationName, &value[2]);
	            	value = childElement->Attribute("d");
	            	assert(value != NULL);
	            	//cout << value <<endl;
	            	string string_tmp = string(value).substr(0, string(value).size()-1);
	            	stationListOut.push_back(make_pair(string(stationName), stringToPath(string_tmp)[0]));
	                num_station ++;
	            }
	        }
	    }
	    cout <<"Number of stations: "<<num_station<<endl;
	}

	StationPath
	SvgPath::
	getStationPath(string pathNameIn){
		StationPath station_path_;
	    TiXmlElement* svgElement;
	    svgElement = svg_doc_->FirstChildElement();
	    TiXmlNode* svg_node_;
	    for ( svg_node_ = svgElement->FirstChild(); svg_node_ != 0; svg_node_ = svg_node_->NextSibling()){
	        TiXmlElement* childElement= svg_node_->ToElement();
	        if( strcmp(childElement->Value(),"path")==0 ){
	            const char* value = childElement->Attribute("id");
	            assert( value!=NULL );
	            if( strcmp(pathNameIn.c_str(),value)== 0 ){
	            	value = childElement->Attribute("d");
	            	assert(value != NULL);
	            	station_path_.point_set_ = stringToPath(string(value));
	            	station_path_.distance_ = getPathLength(station_path_.point_set_);
	            	station_path_.congestion_ = 5.0;
	            	station_path_.speed_limit_ = 2.0;
	            	station_path_.exist_ = true;
	            }
	        }
	    }
	    return station_path_;
	}

	bool
	SvgPath::
	checkStationPath(string pathNameIn){
		StationPath station_path_;
	    TiXmlElement* svgElement;
	    svgElement = svg_doc_->FirstChildElement();
	    TiXmlNode* svg_node_;
	    for ( svg_node_ = svgElement->FirstChild(); svg_node_ != 0; svg_node_ = svg_node_->NextSibling()){
	        TiXmlElement* childElement= svg_node_->ToElement();
	        if( strcmp(childElement->Value(),"path")==0 ){
	            const char* value = childElement->Attribute("id");
	            assert( value!=NULL );
	            if( strcmp(pathNameIn.c_str(),value)== 0 ){
	            	return true;
	            }
	        }
	    }
	    return false;
	}

	vector<string>
	SvgPath::
	SplitString(const char *stringIn, const char* delimiter){
		char *str = strdup(stringIn);
		char *pch = strtok(str, delimiter);
		vector<string> data_s;
		while (pch != NULL){
			data_s.push_back(string(pch));
			pch = strtok(NULL, delimiter);
		}
		free(str);
		return data_s;
	}

//Modify from Demian's code
	vector<PathPoint>
	SvgPath::
	stringToPath(string stringIn){
		vector<string> data_s = SplitString(stringIn.c_str(), " ,");
		bool abs_rel;
		if(data_s[0].find_first_of("Mm")==string::npos){
			throw (string("Unexpected data start character, expected M or m but received ")+data_s[0]);
		}
		else{
			if(data_s[0].find_first_of("M")!=string::npos)
				abs_rel = true;
			else
				abs_rel = false;
		}
		vector<PathPoint> point_set;
		PathPoint size = getSize();
		PathPoint init_point(atof(data_s[1].c_str()), atof(data_s[2].c_str()));
		point_set.push_back(init_point);
		for (size_t i = 3 ;i < data_s.size(); i=i+2){
			PathPoint tmp_point;
			if (abs_rel){
				if (strcmp(data_s[i].c_str(), "L") == 0){
					tmp_point.x_ = atof(data_s[i+1].c_str());
					tmp_point.y_ = atof(data_s[i+2].c_str());
					i = i + 2;
				}
				else{
					tmp_point.x_ = atof(data_s[i].c_str());
					tmp_point.y_ = atof(data_s[i+1].c_str());
				}
			}
			else{
				if (strcmp(data_s[i].c_str(), "L") == 0){
					tmp_point.x_ = atof(data_s[i+1].c_str());
					tmp_point.y_ = atof(data_s[i+2].c_str());
					i = i + 2;
				}
				else{
					tmp_point.x_ = point_set.back().x_ + atof(data_s[i].c_str());
					tmp_point.y_ =  point_set.back().y_ + atof(data_s[i+1].c_str());
				}
			}
			point_set.push_back(tmp_point);
		}

		for (vector<PathPoint>::iterator iter = point_set.begin() ; iter != point_set.end(); iter++){
			iter->x_ = iter->x_*resolution_;
			iter->y_ = (size.y_ - iter->y_)*resolution_;
		}
	    return point_set;
	}

	PathPoint
	SvgPath::
	getSize(){
		TiXmlElement* svgElement;
		svgElement = svg_doc_->FirstChildElement();

		double width,height;
		if( svgElement->QueryDoubleAttribute("width", &width)==TIXML_NO_ATTRIBUTE
				|| svgElement->QueryDoubleAttribute("height", &height)==TIXML_NO_ATTRIBUTE ){
			throw ("Height or width information not found");
		}
		PathPoint size;
		size.x_ = width;
		size.y_ = height;
		return size;
	}

#define SQ(x)  ((x)*(x))

	double
	SvgPath::
	getPathLength(vector<PathPoint>& pathPointIn){
		double length = 0;
		for (size_t i = 1; i < pathPointIn.size(); i++){
			length += sqrt(SQ(pathPointIn[i].x_ - pathPointIn[i-1].x_) + SQ(pathPointIn[i].y_ - pathPointIn[i-1].y_));
		}
		return length;
	}

} // namespace Mission_Plan

#endif /* SVGPATH_CPP_ */
