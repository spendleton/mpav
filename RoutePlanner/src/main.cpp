/*
 * main.cpp
 *
 *  Created on: 11 Jul, 2014
 *      Author: liuwlz
 */

#include <RoutePlanner/StationLoading.hpp>
#include <RoutePlanner/StationSeaching.hpp>
#include <RoutePlanner/StationRouting.hpp>
#include <RoutePlanner/StationInteractive.hpp>
#include <RoutePlanner/StationIntegration.hpp>

#define PROMPTENABLE

int main (int argc, char** argv){

	ros::init(argc, argv,"RoutePlanner");
	//MissionPlan::StationIntegration integration;
	//MissionPlan::StationInteractive interactive;
	MissionPlan::StationRouting routing;

#ifdef PROMPTENABLE
	routing.prompt(false);

#else
	cout << argc<<","<<argv[1]<<","<<argv[2]<<endl;
	if (argc != 3){
		cout << "Please start and end stations"<<endl;
		return 0;
	}
	routing.prompt(atoi(argv[1]),atoi(argv[2]));
#endif
	ros::spin();
	return 1;
}
