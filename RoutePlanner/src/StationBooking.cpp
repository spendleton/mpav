/*
 * StationBooking.cpp
 *
 *  Created on: 5 Aug, 2014
 *      Author: liuwlz
 */


#include <RoutePlanner/StationBooking.hpp>

namespace MissionPlan{
	StationBooking::
	StationBooking():
	StationRouting(),
	vehicle_current_station_(-1),
	first_station_init_(false),
	paired_station_init_(false),
	dist_to_goal_(DBL_MAX),
	mission_status_(MissionWaiting){
		while(!getVehiclePose()){
			ROS_INFO("Failed to get vehicle pose, keep trying!");
		}
	}

	StationBooking::
	~StationBooking(){

	}

	bool
	StationBooking::
	getVehiclePose(){
		tf::Stamped<tf::Pose> global_pose;
		global_pose.setIdentity();
		tf::Stamped<tf::Pose> base_pose;
		base_pose.setIdentity();
		base_pose.frame_id_ = base_frame_;
		base_pose.stamp_ = ros::Time();
		ros::Time current_time = ros::Time::now(); // save time for checking tf delay later

		try {
			tf_.waitForTransform(global_frame_,base_frame_,ros::Time::now(),ros::Duration(0.001));
			tf_.transformPose(global_frame_, base_pose, global_pose);
		}
		catch(tf::LookupException& ex) {
			ROS_ERROR("No Transform available Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ConnectivityException& ex) {
			ROS_ERROR("Connectivity Error: %s\n", ex.what());
			return false;
		}
		catch(tf::ExtrapolationException& ex) {
			ROS_ERROR("Extrapolation Error: %s\n", ex.what());
			return false;
		}
		tf::poseStampedTFToMsg(global_pose, vehicle_pose_);
		initFirstStation();
		initPairedStation();
		return true;
	}

	void
	StationBooking::
	reasonMissionStatus(){
		switch (mission_status_) {
			case MissionWaiting:
				this->prompt();
				break;
			case ConfirmPickUp:
				askForPermission(ConfirmPickUp);
				break;
			case ApproachPickUp:
				checkDistToGoal(ApproachPickUp);
				break;
			case ConfirmDest:
				askForPermission(ConfirmDest);
				break;
			case ApproachDest:
				checkDistToGoal(ApproachDest);
				break;
			case ConfirmReach:
				askForPermission(ConfirmReach);
			default:
				break;
		}
	}

	void
	StationBooking::
	askForPermission(int permissionType){
		if (permissionType == ConfirmPickUp){
			cout << "Ready for heading to PickUp Station? (y/n): ";
			if (cin.get() == 'y'){
				mission_status_ = ApproachPickUp;
				publishPath(ApproachPickUp);
			}
			else
				mission_status_ = ConfirmPickUp;
		}
		if (permissionType == ConfirmDest){
			cout << "Ready for heading to Destination? (y/n): ";
			if (cin.get() == 'y'){
				mission_status_ = ApproachDest;
				publishPath(ApproachDest);
			}
			else
				mission_status_ = ConfirmDest;
		}

		if (permissionType == ConfirmReach){
			cout << "Destination Reached! Confirm not leave anything behind? (y/n): ";
			mission_status_ = cin.get() == 'y' ? MissionWaiting : ConfirmReach;
		}
	}

	void
	StationBooking::
	publishPath(int pathType){
		path_.poses.clear();
		station_path_ = pathType == ApproachPickUp ? mission_path_.first : mission_path_.second;
		for (vector<PathPoint>::iterator iter = station_path_.point_set_.begin();
				iter != station_path_.point_set_.end();
				iter++){
			geometry_msgs::PoseStamped current_pose;
			current_pose.pose.position.x = iter->x_;
			current_pose.pose.position.y = iter->y_;
			current_pose.pose.orientation.w = 1.0;
			path_.poses.push_back(current_pose);
		}
		path_.header.frame_id = global_frame_;
		path_.header.stamp = ros::Time::now();
		path_pub_.publish(path_);
	}

	void
	StationBooking::
	checkDistToGoal(int goalType){
		if (dist_to_goal_ < 3.0){
			if (goalType == ApproachPickUp)
				mission_status_ = ConfirmDest;
			else if (goalType == ApproachDest)
				mission_status_ = ConfirmReach;
			else
				return;
		}
	}

	// Here initFistStation is to initialize the current vehicle station when the vehicle is firstly launched.
	void
	StationBooking::
	initFirstStation(){
		double min_dist = DBL_MAX;
		cout << "Station size"<< station_name.size()<<endl;
		for (vector<string>::iterator iter = station_name.begin();
				iter != station_name.end();
				iter++){
			vector<int> neighbour = station_searching_->getStationLoading()->getStationNeighbour(iter - station_name.begin());
			for (vector<int>::iterator iter_inner = neighbour.begin();
					iter_inner!= neighbour.end();
					iter_inner++){

				StationPath station_path_tmp = station_searching_->getRoute(iter - station_name.begin(),*iter_inner);

				for (vector<PathPoint>::iterator iter_pts = station_path_tmp.point_set_.begin();
						iter_pts != station_path_tmp.point_set_.end()-1;
						iter_pts ++){
					double curr_dist = getVehiclePathPointDist(*iter_pts, *(iter_pts+1));
					if (curr_dist < min_dist){
						min_dist = curr_dist;
						vehicle_current_station_ = iter-station_name.begin();
					}
				}
			}
		}
		first_station_init_ = true;
		cout <<"Will start from station: "<< station_name[vehicle_current_station_]<< " and ready for transform"<<endl;
	}

	void
	StationBooking::
	initPairedStation(){
		for (vector<int>::iterator iter = station_index.begin();
				iter!= station_index.end();
				iter++){
			if (strncmp(station_name[*iter].c_str(),"Opp",3)==0)
				continue;
			else{
				bool pair_found = false;;
				StationCouple current_pair;
				current_pair.first = *iter;
				for (vector<int>::iterator iter_opp = station_index.begin();
						iter_opp!= station_index.end();
						iter_opp++){
					if (strncmp(station_name[*iter_opp].c_str(),"Opp",3)==0){
						if (station_name[*iter_opp].compare(3,station_name[*iter].length(),station_name[*iter])==0){
							current_pair.second = *iter_opp;
							pair_found = true;
						}
					}
					else
						continue;
				}
				if (!pair_found){
					current_pair.second = *iter;
				}
				station_paired_.push_back(current_pair);
			}
		}
		paired_station_init_ = true;
	}

	void
	StationBooking::
	prompt(){
		for (vector<StationCouple>::iterator iter = station_paired_.begin();
				iter != station_paired_.end();
				iter++){
			cout << "Station ID: "<< iter - station_paired_.begin() << " Name: " << station_name[iter->first] << endl;
		}

		int start, end;
		cout<<"Please type in the start station: "<<endl;
		cin >> start;
		while (start > (int)station_paired_.size()-1){
			cerr << "Please type in valid start station number"<<endl;
			cin >> start;
		}
		cout<<"Please type in the end station: "<<endl;
		cin >> end;
		while (end > (int)station_paired_.size()-1){
			cerr << "Please type in valid end station number"<<endl;
			cin >> end;
		}
		routeBooking(station_paired_[start], station_paired_[end]);
	}

	void
	StationBooking::
	routeBooking(StationCouple startIn, StationCouple endIn){

		cout << "Selected Start Station: "<<station_name[startIn.first]<< " or "<< station_name[startIn.second] <<endl;

		StationPath start_path_first = station_searching_->getRoute(vehicle_current_station_, startIn.first);
		StationPath start_path_second = station_searching_->getRoute(vehicle_current_station_, startIn.second);

		mission_path_.first = start_path_first.distance_ < start_path_second.distance_ ? start_path_first :start_path_second;
		int pick_up_station = start_path_first.distance_ < start_path_second.distance_ ? startIn.first: startIn.second;

		cout << "Selected End Station: "<<station_name[endIn.first]<< " or "<< station_name[endIn.second] <<endl;
		StationPath end_path_first = station_searching_->getRoute(pick_up_station, endIn.first);
		StationPath end_path_second = station_searching_->getRoute(pick_up_station, endIn.second);

		mission_path_.second = end_path_first.distance_ < end_path_second.distance_ ? end_path_first: end_path_second;
		vehicle_current_station_ = end_path_first.distance_ < end_path_second.distance_ ? endIn.first : endIn.second;
	}

#define SQ(x) ((x)*(x))
	double
	StationBooking::
	getVehiclePathPointDist(const PathPoint& pointInA, const PathPoint& pointInB){
		double path_pts_theta = atan2(pointInB.y_-pointInA.y_, pointInB.x_ - pointInA.x_);
		geometry_msgs::Pose& veh_pose = vehicle_pose_.pose;
		double roll=0, pitch=0, yaw=0;
		tf::Quaternion q;
		tf::quaternionMsgToTF(veh_pose.orientation, q);
		tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
		double angle_dist = fabs(yaw - path_pts_theta);
		double linear_dist = sqrt(SQ(veh_pose.position.x - pointInA.x_)+SQ(veh_pose.position.y - pointInA.y_));
		return (linear_dist + 2*angle_dist);
	}

}  // namespace MissionPlan
