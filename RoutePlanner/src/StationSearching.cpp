/*
 * StationSearching.cpp
 *
 *  Created on: 3 Jul, 2014
 *      Author: liuwlz
 */


#include <RoutePlanner/StationSeaching.hpp>

namespace MissionPlan{

	StationSearching::
	StationSearching(){
		station_loading_ = shared_ptr<StationLoading>(new StationLoading());
		station_loading_->loadProperty(route_map_);
	}

	StationSearching::
	~StationSearching(){

	}

	void
	StationSearching::
	searchRoute(int startIn, int endIn, vector< VertexDpter>& pathOut){

		vector<double> distances(num_vertices(*route_map_));
		vector<VertexDpter> predecessor(num_vertices(*route_map_));

		VertexDpter start= *vertices(*route_map_).first + startIn;
		VertexDpter goal = *vertices(*route_map_).first + endIn;

		//cout << "Start from "<< (*route_map_)[start].name << " to "<< (*route_map_)[goal].name << endl;

		dijkstra_shortest_paths(*route_map_, start,
		      weight_map(get(&Route::distance, *route_map_)).
		      predecessor_map(boost::make_iterator_property_map(predecessor.begin(), get(vertex_index, *route_map_))).
		      distance_map(make_iterator_property_map(distances.begin(), get(vertex_index, *route_map_))));

		VertexDpter current= goal;
		while(current!= start) {
			if (current == predecessor[current]){
				pathOut.clear();
				return;
			}
			pathOut.push_back(current);
		    current = predecessor[current];
		}
		pathOut.push_back(start);

		for (vector< VertexDpter>::iterator iter=pathOut.begin(); iter != pathOut.end(); ++iter) {
			cout << (*route_map_)[*iter].name << "<---";
		}
		cout <<"Current Pose"<< std::endl;

	}

	StationPath
	StationSearching::
	getRoute(int startIn, int endIn){
		StationPath combined_path;
		bool initial_path = true;
		vector<VertexDpter> path;
		searchRoute(startIn, endIn, path);

		if (path.size() == 0){
			combined_path.exist_ = false;
			cout << "No Feasible Path Got" <<endl;
			return combined_path;
		}
		assert(path.size()!=0);
		for (vector<VertexDpter>::iterator iter = path.end()-1; iter != path.begin(); iter--){
			StationPath current_station_path = station_loading_->getStationPath((*route_map_)[*iter].id, (*route_map_)[*(iter-1)].id);
			if (RoutePlanDebug) cout <<"Partial name: "<< station_loading_->getRouteName((*route_map_)[*iter].id, (*route_map_)[*(iter-1)].id) <<" Path Length: " << current_station_path.distance_ << endl;
			if (initial_path){
				combined_path =current_station_path;
				initial_path = false;
			}
			else{
				combined_path = combined_path + current_station_path;
			}
		}
		if (combined_path.exist_){
			cout <<"Feasible Path Got"<<endl;
			cout << "Path Lenght: "<<combined_path.distance_<<endl;
			cout << "Path size: "<<combined_path.point_set_.size()<<endl;
		}
		return combined_path;
	}

}  // namespace MissionPlan
