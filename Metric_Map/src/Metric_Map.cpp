/*
 * Metric_Map.cpp
 *
 *  Created on: 27 Jul, 2014
 *      Author: liuwlz
 */


#include <Metric_Map/Metric_Map.hpp>

namespace Perception {
	MetricMap::
	MetricMap():
	pri_nh_("~"),
	prior_map_init_(false),
	obst_map_init_(false),
	prior_info_init_(false){

		scan_fusion_ = boost::shared_ptr<ScanFusion>(new ScanFusion());
		dist_metric_ = boost::shared_ptr<DistMetric>(new DistMetric());
		prior_map_ = boost::shared_ptr<nav_msgs::OccupancyGrid>(new nav_msgs::OccupancyGrid());
		prior_pts_ = boost::shared_ptr<sensor_msgs::PointCloud>(new sensor_msgs::PointCloud());
		obst_map_ = boost::shared_ptr<nav_msgs::OccupancyGrid>(new nav_msgs::OccupancyGrid());
		metric_map_ = boost::shared_ptr<Metric_Map::MetricMapMsg>(new Metric_Map::MetricMapMsg());

		pri_nh_.param("global_frame",global_frame_, string("map"));
		pri_nh_.param("base_frame",base_frame_,string("base_link"));
		pri_nh_.param("resolution",resolution_,0.2);
		pri_nh_.param("width",width_,200);
		pri_nh_.param("height",height_,100);

		metric_map_timer_ = nh_.createTimer(ros::Duration(0.1),&MetricMap::updateMetricMap, this);
		obst_map_pub_ = nh_.advertise<nav_msgs::OccupancyGrid>("obst_map",1);
		prior_pts_pub_ = nh_.advertise<sensor_msgs::PointCloud>("prior_pts",1);
		metric_pts_pub_ = nh_.advertise<sensor_msgs::PointCloud>("metric_pts",1);
		metric_map_pub_ = nh_.advertise<Metric_Map::MetricMapMsg>("metric_map",1);

		initObstMap();
	}

	MetricMap::
	~MetricMap(){

	}

	void
	MetricMap::
	initObstMap(){
		obst_map_->info.height = height_;
		obst_map_->info.width = width_;
		obst_map_->info.resolution = resolution_;
		obst_map_->info.origin.position.x = -width_*resolution_/2.0;
		obst_map_->info.origin.position.y = -height_*resolution_/2.0;
		obst_map_->info.origin.orientation.w = 1.0;
		obst_map_->header.frame_id = base_frame_;
		obst_map_->data.resize(height_*width_);

		metric_map_->info = obst_map_->info;
		metric_map_->header = obst_map_->header;
		metric_map_->metric.resize(height_*width_);
		obst_map_init_ = true;
	}

	void
	MetricMap::
	initPriorMap(){
		assert(false);
	}

	void
	MetricMap::
	updatePriorInfo(){
		assert(false);
	}

	void
	MetricMap::
	addPointToObstMap(nav_msgs::OccupancyGrid& obstMap, geometry_msgs::Point32& pointIn, int obstValue){
		int map_index_x = (int)((pointIn.x - obstMap.info.origin.position.x)/obstMap.info.resolution);
		if (map_index_x < 0 || map_index_x > (int)obstMap.info.width-1 )
			return;
		int map_index_y = (int)((pointIn.y - obstMap.info.origin.position.y)/obstMap.info.resolution);
		if (map_index_y < 0 || map_index_y > (int)obstMap.info.height-1)
			return;
		int map_index = map_index_x + map_index_y*obstMap.info.width;
		obstMap.data[map_index] = obstValue;
	}

	void
	MetricMap::
	updateObstMap(){
		if (DEBUG)
			cout << "Enter obstacel map function"<<endl;
		obst_map_->data.clear();
		obst_map_->data.resize(height_*width_,100);
		sensor_msgs::PointCloudPtr prior_local(new sensor_msgs::PointCloud());
		prior_pts_->header.stamp = ros::Time();
		try{
			tf_.waitForTransform(base_frame_, global_frame_, ros::Time(0), ros::Duration(0.01));
			tf_.transformPointCloud(base_frame_,*prior_pts_,*prior_local);
		}
		catch (tf::TransformException &ex){
			ROS_WARN("Failure %s\n", ex.what());
			return;
		}
		for (vector<geometry_msgs::Point32>::iterator iter = prior_local->points.begin();
				iter != prior_local->points.end();
				iter++){
			addPointToObstMap(*obst_map_,*iter, 0);
		}
		sensor_msgs::PointCloud scan_pts = scan_fusion_->getScanPts();
		for (vector<geometry_msgs::Point32>::iterator iter = scan_pts.points.begin();
				iter != scan_pts.points.end();
				iter++){
			addPointToObstMap(*obst_map_,*iter, 100);
		}
		if (DEBUG)
			cout << "Publish obst map"<<endl;
		obst_map_pub_.publish(obst_map_);
	}

	void
	MetricMap::
	updateDistMap(){
		int map_index, map_x, map_y;
		sensor_msgs::PointCloud metric_map_pts;
		sensor_msgs::ChannelFloat32 dist_chanel;
		dist_metric_->getDistMap(*obst_map_,*metric_map_);

		for (vector<double_t>::iterator iter = metric_map_->metric.begin();
				iter != metric_map_->metric.end();
				iter++){
			map_index = iter - metric_map_->metric.begin();
			map_x = map_index%metric_map_->info.width;
			map_y = map_index/metric_map_->info.width;

			geometry_msgs::Point32 map_pose;
			map_pose.x = map_x*metric_map_->info.resolution + metric_map_->info.origin.position.x;
			map_pose.y = map_y*metric_map_->info.resolution + metric_map_->info.origin.position.y;

			dist_chanel.name = "dist";
			dist_chanel.values.push_back(*iter);

			metric_map_pts.points.push_back(map_pose);
		}
		metric_map_pts.channels.push_back(dist_chanel);
		metric_map_pts.header.frame_id = base_frame_;
		metric_map_pts.header.stamp = ros::Time::now();
		metric_pts_pub_.publish(metric_map_pts);

		metric_map_->header.stamp = ros::Time::now();
		metric_map_pub_.publish(*metric_map_);
	}

	void
	MetricMap::
	updateMetricMap(const ros::TimerEvent& e){
		if (!obst_map_init_ || !prior_map_init_)
			return;
		if (!prior_info_init_ && reference_path_got_)
			updatePriorInfo();
		ROS_INFO_ONCE("Metric Map Timer Spinning");
		prior_pts_->header.stamp = ros::Time::now();
		prior_pts_->header.frame_id = global_frame_;
		prior_pts_pub_.publish(*prior_pts_);
		updateObstMap();
		updateDistMap();
	}
}  // namespace Perception
