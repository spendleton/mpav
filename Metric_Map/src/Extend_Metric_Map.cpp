/*
 * Extend_Metric_Map.cpp
 *
 *  Created on: 30 Jul, 2014
 *      Author: liuwlz
 */

#include <Metric_Map/Extend_Metric_Map.hpp>

namespace Perception {
	ExtendMetricMap::
	ExtendMetricMap():
	MetricMap(){
		pri_nh_.param("extend_dist",extend_dist_,16.0);
		ref_path_dist_ = boost::shared_ptr<DistMetric>(new DistMetric());
		path_dist_map_ = boost::shared_ptr<Metric_Map::MetricMapMsg>(new Metric_Map::MetricMapMsg());

		reference_path_sub_ = nh_.subscribe("route_plan",1,&ExtendMetricMap::referencePathCallBack, this);

		reference_path_got_ = false;
		initPriorMap();
	}

	ExtendMetricMap::
	~ExtendMetricMap(){

	}

	void
	ExtendMetricMap::
	initPriorMap(){
		ROS_INFO("Init Extend Prior Points");
		nav_msgs::GetMap::Request  request;
		nav_msgs::GetMap::Response response;
		ROS_INFO("Requesting the static_map prior map...");
		while(!ros::service::call("/static_map", request, response)){
			ROS_WARN("Extended Metric: Request for map failed; trying again...");
			ros::Duration sleep(2);
			sleep.sleep();
			ros::spinOnce();
		}
		*prior_map_ = response.map;
		prior_map_init_ = true;
		ROS_INFO("static_map map retrieved...");
	}

	void
	ExtendMetricMap::
	initReferencePath(const nav_msgs::Path& pathIn){
		ROS_INFO("Init Reference Path");
		reference_path_ = pathIn;
		densePathSegment();
		reference_path_got_ = true;
	}

	void
	ExtendMetricMap::
	referencePathCallBack(const nav_msgs::Path& pathIn){
		ROS_INFO("Callback Reference Path");
		reference_path_ = pathIn;
		densePathSegment();
		reference_path_got_ = true;
	}

	void
	ExtendMetricMap::
	densePathSegment(){
		vector<geometry_msgs::PoseStamped> path_dense_tmp = reference_path_.poses;
		reference_path_.poses.clear();

		for (vector<geometry_msgs::PoseStamped>::iterator iter = path_dense_tmp.begin();
				iter != path_dense_tmp.end()-1;
				iter ++){
			reference_path_.poses.push_back(*iter);
			int path_seg_size = (int)(distBetweenPose(*iter,*(iter+1)) / extend_dist_) + 1;
			if (path_seg_size > 1){
				vector<geometry_msgs::PoseStamped> pose_set_insert(path_seg_size - 1);
				for (vector<geometry_msgs::PoseStamped>::iterator iter_insert = pose_set_insert.begin();
						iter_insert != pose_set_insert.end();
						iter_insert ++){
					int curr_index = iter_insert - pose_set_insert.begin() + 1;
					iter_insert->pose.position.x = iter->pose.position.x +
							(double)curr_index/(double)path_seg_size *
							((iter+1)->pose.position.x - iter->pose.position.x);
					iter_insert->pose.position.y = iter->pose.position.y +
							(double)curr_index/(double)path_seg_size *
							((iter+1)->pose.position.y - iter->pose.position.y);
				}
				reference_path_.poses.insert(reference_path_.poses.end(), pose_set_insert.begin(), pose_set_insert.end());
			}
		}
	}

	double
	ExtendMetricMap::
	distBetweenPose(const geometry_msgs::PoseStamped& poseA, const geometry_msgs::PoseStamped& poseB){
		double x_A = poseA.pose.position.x, y_A = poseA.pose.position.y;
		double x_B = poseB.pose.position.x, y_B = poseB.pose.position.y;
		return sqrt((x_A-x_B)*(x_A-x_B)+(y_A-y_B)*(y_A-y_B));
	}

	void
	ExtendMetricMap::
	updatePriorInfo(){
		ROS_INFO("Update Extend Prior Info");
		int map_index , map_x, map_y;
		prior_pts_->points.clear();
		extendReferencePath();
		for (vector<double_t>::iterator iter = path_dist_map_->metric.begin();
				iter != path_dist_map_->metric.end();
				iter++){

			if (*iter > extend_dist_)
				continue;
			map_index = iter - path_dist_map_->metric.begin();

			map_x = map_index%path_dist_map_->info.width;
			map_y = map_index/path_dist_map_->info.width;

			geometry_msgs::Point32 map_pose;
			map_pose.x = map_x*path_dist_map_->info.resolution + path_dist_map_->info.origin.position.x;
			map_pose.y = map_y*path_dist_map_->info.resolution + path_dist_map_->info.origin.position.y;

			prior_pts_->points.push_back(map_pose);
		}
		prior_info_init_ = true;
	}

	void
	ExtendMetricMap::
	extendReferencePath(){
		ROS_INFO("Extend Reference Path");
		path_dist_map_->info = prior_map_->info;
		path_dist_map_->header = prior_map_->header;
		prior_map_->data.clear();
		prior_map_->data.resize(prior_map_->info.width*prior_map_->info.height, 0);
		path_dist_map_->metric.clear();
		path_dist_map_->metric.resize(path_dist_map_->info.width*path_dist_map_->info.height);
		for (vector<geometry_msgs::PoseStamped>::iterator iter = reference_path_.poses.begin();
				iter != reference_path_.poses.end();
				iter++){
			geometry_msgs::Point32 current_pts;
			current_pts.x = iter->pose.position.x;
			current_pts.y = iter->pose.position.y;
			addPointToObstMap(*prior_map_,current_pts,OBST_VALUE);
		}
		cout << (int)(extend_dist_/prior_map_->info.resolution)<<endl;
		ref_path_dist_->getDistMap(*prior_map_,*path_dist_map_);
	}
}  // namespace Perception

