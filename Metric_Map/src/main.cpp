/*
 * main.cpp
 *
 *  Created on: 28 Jul, 2014
 *      Author: liuwlz
 */



#include <Metric_Map/Scan_Fusion.hpp>
#include <Metric_Map/Metric_Map.hpp>
#include <Metric_Map/Prior_Metric_Map.hpp>

using namespace Perception;

int main(int argc, char**argv){
	ros::init(argc, argv, "Metric_Map");
	//ScanFusion scan_fuse;
	PriorMetricMap map;
	ros::spin();
	return 0;
}
