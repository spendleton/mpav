################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
OBJS += \
./src/BucketedQueue.o \
./src/Dist_Map.o \
./src/Extend_Metric_Map.o \
./src/Metric_Map.o \
./src/Prior_Metric_Map.o \
./src/Scan_Fusion.o \
./src/main.o 

CPP_DEPS += \
./src/BucketedQueue.d \
./src/Dist_Map.d \
./src/Extend_Metric_Map.d \
./src/Metric_Map.d \
./src/Prior_Metric_Map.d \
./src/Scan_Fusion.d \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


