/*
 * Scan_Fusion.hpp
 *
 *  Created on: 28 Jul, 2014
 *      Author: liuwlz
 */

#ifndef SCAN_FUSION_HPP_
#define SCAN_FUSION_HPP_

#include <ros/ros.h>
#include <ros/console.h>

#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud.h>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
#include <laser_geometry/laser_geometry.h>

#include <boost/shared_ptr.hpp>

using namespace std;
using namespace boost;

#define DEBUG false

namespace Perception {
	class ScanFusion{
	public:
		ScanFusion();
		virtual ~ScanFusion();

		const sensor_msgs::PointCloud getScanPts()const {return laser_pts_fused_;}

	private:

		ros::NodeHandle nh_, pri_nh_;
		ros::Timer scan_update_timer_;
		ros::Publisher scan_pts_pub_;

		tf::TransformListener tf_;
		laser_geometry::LaserProjection projector_;

		tf::MessageFilter<sensor_msgs::LaserScan> *laser_filter_front_, *laser_filter_rear_;
		message_filters::Subscriber<sensor_msgs::LaserScan> laser_sub_front_, laser_sub_rear_;
		sensor_msgs::PointCloud laser_pts_front_, laser_pts_rear_, laser_pts_fused_;

		bool scan_rear_got_, scan_front_got_;
		string global_frame_, base_frame_;

		void initScanPts();
		void laserFrontCallBack(const sensor_msgs::LaserScanConstPtr scanIn);
		void laserRearCallBack(const sensor_msgs::LaserScanConstPtr scanIn);
		void updateScanPts(const ros::TimerEvent& e);

	};
}  // namespace Perception

#endif /* SCAN_FUSION_HPP_ */
