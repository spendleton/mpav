/*
 * distance_metric.h
 *
 *  Created on: Apr 12, 2013
 *      Author: liuwlz
 */

/* Generate metric map based on the distance to obstacle, i.e. road curb, vehicle or pedestrain
 *
 *Algorithm is based on the dynamic ros voronoi package http://www.ros.org/wiki/dynamicvoronoi
 */

#ifndef DISTANCE_METRIC_H_
#define DISTANCE_METRIC_H_

#include <ros/ros.h>
#include <limits.h>
#include <nav_msgs/OccupancyGrid.h>
#include <sensor_msgs/PointCloud.h>
#include <queue>

#include <Metric_Map/BucketedQueue.hpp>
#include <Metric_Map/MetricMapMsg.h>

#define invalidObstID SHRT_MAX;
#define OBST_VALUE 100

using namespace std;

namespace Perception{

	class DistMetric{
	private:
		struct mapPts{
			double dist, sqdist;
			int obstX, obstY;
			bool fwProcessed;
			bool needRaise;
			bool isSurround;
			bool norminalLane;
			bool midRroad;
		};

		BucketPrioQueue open;
		vector<bool> obsMap;
		vector<mapPts> ptsCell;
//		vector<INTPOINT> addList;
		int width_, height_;
		int Max_Dist_;

		nav_msgs::OccupancyGrid obst_map_;

	public:
		DistMetric();
		~DistMetric();

		void initObstacle();
		bool checkNearestEightCells(int x, int y);
		void setProcessedObst(int x, int y);
		bool isOccupied(int x, int y, mapPts pts);

		void updateDistMap();
		void setMaxDist(int dist){Max_Dist_ = dist;}
		void getDistMap(nav_msgs::OccupancyGrid& obst_map_in_, Metric_Map::MetricMapMsg &dist_map_out);
		void getDistMap(nav_msgs::OccupancyGrid& obst_map_in_, vector<double> &dist_map_out);

	};
}
#endif /* DISTANCE_METRIC_H_ */
