/*
 * Speed_Smoother.hpp
 *
 *  Created on: 13 Aug, 2014
 *      Author: liuwlz
 */

#ifndef SPEED_SMOOTHER_HPP_
#define SPEED_SMOOTHER_HPP_

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Bool.h>

#include <MPAVUtil/Filter.hpp>

using namespace boost;
using namespace std;
using namespace Util;

#define SmootherDebug false

namespace Control {

	class SpeedSmoother {
	public:

		SpeedSmoother();
		virtual ~SpeedSmoother();

	private:
		ros::NodeHandle nh_, pri_nh_;
		ros::Subscriber cmd_steer_sub_, odom_sub_, auto_mode_sub_;
		ros::Publisher cmd_vel_pub_;
		ros::Timer speed_smooth_timer_;

		geometry_msgs::Twist cmd_vel_;

		shared_ptr<Util::LowPassFilter> vel_filter_;
		ros::Time odom_filter_time_, cmd_steer_filter_time_;

		bool auto_mode_;
		double current_vel_;
		double vel_callback_timeout_;
		double max_acc_, max_dec_;

		void speedSmoothTimer(const ros::TimerEvent& e);
		void cmdSteerCallBack(const geometry_msgs::TwistPtr cmdSteerIn);
		void odomCallBack(const nav_msgs::OdometryPtr odomIn);
		void autoModeCallBack(const std_msgs::Bool autoModeIn);
		void speedSmooth(double speedIn, double& smoothedSpeed);
	};

}  // namespace Control


#endif /* SPEED_SMOOTHER_HPP_ */
