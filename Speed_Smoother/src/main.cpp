/*
 * main.cpp
 *
 *  Created on: 21 Aug, 2014
 *      Author: liuwlz
 */

#include <ros/ros.h>
#include <Speed_Smoother/Speed_Smoother.hpp>

int main(int argc, char** argv){
	ros::init(argc, argv, "speed_smoother");
	Control::SpeedSmoother speed_smoother;
	ros::spin();
}
